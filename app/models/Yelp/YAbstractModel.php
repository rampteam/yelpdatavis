<?php

namespace Yelp\Model;

use Yelp\MongoDB\Aggregation;
/**
 * Abstract model
 */
abstract class YAbstractModel extends YBaseQuery{

    protected $validationErrors = array();

    /**
    * Very important this will set the default name of the connection
    **/
    protected $connection = 'mongodb';

    /**
     * Init model
     * @param array $attributes
     * @return boolean
     */
    public function __construct(array $attributes = array())
    {
        
        $this->collection = static::COLL_NAME;
              
        parent::__construct($attributes);

    }
    
}