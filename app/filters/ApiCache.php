<?php 

namespace Yelp\Filter;

use \Cache;
use \Config;

class ApiCache
{
	
	const KEY_PREFIX = 'apicache';
	const DELIMITER = ':';


	public function filter($route, $request, $response = null)
	{	

		if(!$this->cacheEnabled()) return;
		if(is_null($response))
		{	
			$fromCache = $this->before();

			if($fromCache !== false)
			{	
				$response = \Response::make($fromCache, 200);
				$response->header('Cache', "Hit");
				$response->header('Content-Type', "application/json");
				return $response;
			}

		}else
		{		
			$this->after($response);
		}

		
	}

	public function before()
	{
		$fullCacheKey = $this->getCacheKey();

		if (Cache::has($fullCacheKey)) 
		{	
			//Set cache header
			return Cache::get($fullCacheKey);
		}

		return false;
	}

	public function after($response)
	{	
		if(!$this->cacheEnabled()) return;
		//Cache only if status code is 200
		if($response->isSuccessful())
		{	
			Cache::forever($this->getCacheKey(), $response->getContent());
		}
		
	}

	protected function cacheEnabled()
	{
		return Config::get('cache.enableApiCache', false);
	}

	protected function getCacheKey()
	{	
		$key = md5('route-'.\Str::slug(\Request::fullUrl()));
		$hashKey = self::KEY_PREFIX.self::DELIMITER.$key;

		return $hashKey;
	}
}