<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/tst',  function () {
	
	$m = new Yelp\MongoDB\DataInsertion\Bootstrap("some file",\Config::get('database.connections.mongodb'));
	$m->insertData();
});

Route::get('/', array('before' => 'yauth.app', function () {
	return View::make('layouts.master');
}));

Route::group(array( 'before' => 'auth.app'), function() {
	
});


Route::group(array('before' => 'auth.app'), function(){
    
	Route::resource('users', 'UsersController', array('except' =>array('index', 'destroy')));

	Route::get('login', 'AuthController@getLogin');
	Route::post('login', 'AuthController@postLogin');	
	Route::get('logout', 'AuthController@getLogout');	

});


Route::group(array('prefix' => 'api', 'before' => 'auth.api|cache', 'after' => 'cache'), function () {

	Route::get('ybusinesses/states', 'Api\YBusinessesController@getStates');
	
	Route::get('ybusinesses/treemap', 'Api\YBusinessesController@getTreemap');

	Route::get('ybusinesses/timereviews', 'Api\YBusinessesController@getTimeFrameReviews');

	Route::get('ybusinesses/geoQuery', 'Api\YBusinessesController@getGeo');

	Route::get('yusers/fgraph', 'Api\YUsersController@getUserFriends');
	
	Route::get('yusers/fgraph/{userId}', 'Api\YUsersController@getUserFriendsFor');

	Route::get('yusers/businesses', 'Api\YUsersController@getUserBusinesses');

	Route::get('yusers/reviewstats', 'Api\YUsersController@getUserReviewStats');
	Route::get('yusers/timereviews', 'Api\YUsersController@getTimeFrameReviews');

	Route::get('yusers/reviewstars', 'Api\YUsersController@getUserStars');
	
	Route::resource('yusers', 'Api\YUsersController', array('only' => array('index', 'show')));
	Route::resource('ybusinesses', 'Api\YBusinessesController', array('only' => array('index', 'show')));
	Route::resource('yreviews', 'Api\YReviewsController', array('only' => array('index', 'show')));
});