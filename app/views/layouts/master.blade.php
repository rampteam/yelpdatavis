<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />


        <title>
            @section('title')
                Yelp Datavis 
            @show   
        </title>

        @section('head-rel')
            <link rel="stylesheet" href="/static/css/master.min.css" />
        @show

        @section('head-scripts')
            <script src="/static/js/headLibrary.min.js"></script>
        @show
    </head>
<body>
	<div id="loadingModal" class="reveal-modal spinner-modal" data-reveal data-options="close_on_background_click:true">
		 <div class="csspinner traditional"></div>
	</div>
    <div class="row">
      <div class="top-bar-wrapper large-12 columns fixed contain-to-grid">
        @section('page-header')
            <nav class="top-bar" data-topbar data-options="is_hover: false;">
                <ul class="title-area">
                  <li class="name">
                <h1><a href="#">Yelp Datavis</a></h1>
                  </li>
                  <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                </ul>

            <section class="top-bar-section">
                <!-- Right Nav Section -->
                <ul class="right">
                  @if (!YAuth::check())
                    <li class=""><a href="{{ URL::action('AuthController@getLogin') }}">Login</a></li>
                  @endif
                  <li class="has-dropdown">
                    <a href="#" class="dropdown-name" data-options="is_hover: false;">
                    	Hello, {{{ YAuth::user()['firstName']}}} {{{ YAuth::user()['lastName'] }}}
                    </a>
                    <ul class="dropdown text-center">
                      <li><a href="{{ URL::action('UsersController@edit', array(YAuth::user()['id'])) }}">Edit Account</a></li>
                      <li><a href="{{ URL::action('AuthController@getLogout') }}">Logout</a></li>
                    </ul>
                  </li>
                </ul>
            </section>
            </nav>
        @show
      </div>
    </div>
    <div class="row content page-wrapper">
        <div class="large-12 columns">
            <div class="row">
                <div class="large-3 columns">
                    @section('side-bar')
                    	<div class="side-header large-12 columns">
                       		<dl class="graphLinks tabs centered" data-tab class="">
                       			<dt></dt>
                       			<dd>
                       				<a href="#usersTab">Users</a>
                       			</dd>
                       			<dd>
                       				<a href="#reviewsTab">Reviews</a>
                       			</dd>
                       			<dd>
                       				<a href="#businessesTab">Businesses</a>
                       			</dd>
                            	<dd>
                            	  <a href="#businessesMapsTab">Maps</a>
                            	</dd>
                       		</dl>
                       	</div>
                       <div class="tabs-content filter-bar">
                       	<div class="content active" id="businessesTab"></div>
                       	<div class="content" id="usersTab"></div>
                        <div class="content" id="reviewsTab"></div>
                       	<div class="content" id="businessesMapsTab"></div>
                       </div>
                    @show
                </div>
                <div id="content" class="large-9 columns">
                    @section('content')

                    @show
                </div>
            </div>
        </div>
    </div>
    <div class="row page-footer">
        <div class="large-12 columns">
            @section('page-footer')
            	<div class="centered">
            		<div class="license-img">
                		<a target="_blank" rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
                			<img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
                		</a>
                	</div>
                </div>
                <div class="text-center">
                <p class="authors-footer">        <span property="dct:title" rel="dct:type">
                Yelp Datavis
                </span> by 
                <a target="_blank" href="https://bitbucket.org/rampteam/YelpDatavis" property="cc:attributionName" rel="cc:attributionURL">
                Alex Porcescu, Robert Maftei
                </a> </p>
            
                <p class="footer-idea">Ideea from: 
                <a target="_blank" href="http://www.yelp.co.uk/dataset_challenge/" rel="dct:source">http://www.yelp.co.uk/dataset_challenge/</a></p>
            	</div>
            @show
        </div>
    </div>
    @section('foot-scripts')


        <script type="text/javascript"
      		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfMSXSTCO_QH2ZF7ypYYgjWvfcDwQno_g&amp;sensor=TRUE">
    	</script>

        <script id="businessTemplate" type="text/template">
        	@include('templates.business')
        </script>

         <script id="businessMapTooltip" type="text/template">
          @include('templates.businessMapTooltip')
        </script>

        <script id="userMapTooltip" type="text/template">
          @include('templates.userMapTooltip')
        </script>

        <script id="userTemplate" type="text/template">
        	@include('templates.user')
        </script>
        <script id="userFriendsTemplate" type="text/template">
        	@include('templates.userFriends')
        </script>
        <script id="optionTemplate" type="text/template">
        	@include('templates.option')
        </script>
        <script id="numericFilterTemplate" type="text/template">
        	@include('templates.numericFilter')
        </script>

        <script id="reviewTemplate" type="text/template">
          @include('templates.review')
        </script>

        <script id="404Template" type="text/template">
          @include('templates.404')
        </script>

        <script id="indexModal" type="text/template">
          @include('templates.indexModal')
        </script>

        <script id="userGraphModal" type="text/template">
          @include('templates.userGraphModal')
        </script>

        <script src="/static/js/footerLibrary.min.js"></script>
        <script src="/static/js/master.min.js"></script>


        <script>
            $(document).foundation();
        </script>
        <script type="text/javascript">
        	// set api token
        	$.ajaxSetup({
        		'headers': {
        			'X-Auth-Token': '{{ YAuth::getToken() }}',
        		}
        	});	
        	$(document).ajaxError(function(event, xhr, settings, thrownError) {
        		if(xhr.status === 401)
        		{
        			window.location.replace('{{ URL::action("AuthController@getLogin") }}');
        		}
        		else if(xhr.status === 404)
        		{
        			location.hash = '#NotFound';
        		}
        	});	
        </script>
    @show
</body>
</html>