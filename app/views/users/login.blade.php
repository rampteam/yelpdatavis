@extends('layouts.users')

@section('content')
	<div class="row">
		<div class="large-8 columns medium-centered">
			{{ Form::open(array('action' => 'AuthController@postLogin', 'method' => 'post')) }}
				<fieldset>
					<legend>Login</legend>
					{{ Form::email('email', null, array('placeholder' => 'Email')) }}
					{{ Form::password('password', array('placeholder' => 'Password')) }}
					{{ Form::submit('Login', array('class' => 'button')) }}

					@if (!YAuth::check())
						<a href="{{ URL::action('UsersController@create' ) }}">
						<div class="right button register-button">
							Don&#39;t have an account? 
							<br/>
							Register Here
						</div>
					</a>
					@endif
				</fieldset>
			{{ Form::close() }}
		</div>
	</div>
@stop