@extends('layouts.users')

@section('errors')
@stop

@section('content')
	<div class="large-8 columns medium-centered">
		<fieldset>
			<legend>Register</legend>
				{{ Form::open(array('action' => 'UsersController@store', 'method' => 'post')) }}	
					{{ Form::label('firstName', 'First Name') }}
					{{ Form::text('firstName', Input::old('firstName', null), array('placeholder' => 'First Name')) }}
					{{ $errors->first('firstName', '<small class="error text-center" data-abide>:message</small>')}}

					{{ Form::label('lastName', 'Last Name') }}
					{{ Form::text('lastName', Input::old('lastName', null), array('placeholder' => 'Last Name')) }}
					{{ $errors->first('lastName', '<small class="error text-center" data-abide>:message</small>')}}

					{{ Form::label('email', 'Email') }}
					{{ Form::email('email', Input::old('email',null), array('placeholder' => 'Email')) }}
					{{ $errors->first('email', '<small class="error text-center" data-abide>:message</small>')}}
					
					{{ Form::label('password1', 'Passowrd') }}
					{{ Form::password('password1', array('placeholder' => 'Password (min 6 characters)')) }}
					{{ $errors->first('password1', '<small class="error text-center" data-abide>:message</small>')}}
					
					{{ Form::label('password2', 'Retype password') }}
					{{ Form::password('password2', array('placeholder' => 'Retype password')) }}
					{{ $errors->first('password2', '<small class="error text-center" data-abide>:message</small>')}}
					
					{{ Form::checkbox('terms', 'agree') }}
					<span><a href="#">I Agree To The Terms &amp; Conditions</a></span>
					{{ $errors->first('terms', '<small class="error text-center" data-abide>:message</small>')}}
					
					<br/>
					{{ Form::submit('Register', array('class' => 'button')) }}
				{{ Form::close() }}
		</fieldset>
	</div>
@stop