@if ($errors->any())
	<div class="large-8 columns medium-centered">
		    {{ implode('', $errors->all('<small class="error text-center" data-abide>:message</small>')) }}
	</div>
@endif