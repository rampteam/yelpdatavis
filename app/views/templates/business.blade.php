<div class="row business-name text-center">
	<h2><%=name%></h2>
	<span><%=full_address%></span>
</div>
<div class="categories text-center margin-top-3"><%=categories%></div>
<div class="rating row margin-top-3">
	<div class="large-12 columns small-centered text-center">
        <div class="stars large-6 columns">
        	<% for (var i = 1; i <= 5; i++) { %>
        		<% if(i <= stars ) { %>
        			<span class="star-icon full">☆</span>
        		<% } else if ( i - 0.5 === stars ) { %>
        			<span class="star-icon half">☆</span>
        		<% } else { %>
        			<span class="star-icon">☆</span>
        		<% } %>	
        	<% } %>
        </div>
        <div id="businessReviews" class="review-count large-6 columns review-count">
        	<span class="active hover-pointer">
        		<%=review_count%> Reviews
        	</span>
        </div>
    </div>
</div>
<div class="row margin-top-3">
    <div class="large-6 columns ">
        <a class="business-map-trigger"><img class="centered" src="http://maps.googleapis.com/maps/api/staticmap?center=<%=latitude%>,<%=longitude%>&amp;zoom=9&amp;size=200x200&amp;sensor=false&amp;markers=color%3ablue%7clabel%3aLocation`%7c<%=latitude%>%2c<%=longitude%>"></a>
        <div class="centered margin-top-3">
        	<div class="button-group">
       			<a id="city" class="tiny button secondary" href="<%=cityRoute%>">
       				City: <span class="active"><%=city%></span>
       			</a>
        		<a id="state" class="tiny button secondary" href="<%=stateRoute%>">
        			State: <span class="active"><%=state%></span>   	
        		</a>
        	</div>
        </div>
	</div>
	<div class="large-6 columns">
		<img class="centered" src="<%=staticImage%>" alt="static/images/no-image.png"/>
	</div>
</div>
<div class="row">
	<% if(hours.length !== 0) { %>
    	<div class="row text-center">
    		<h5>Program</h5>
    	</div>
    	<div class="row">
    			<% for(var day in hours) { %>
    				<div class="row">
    					<div class="large-6 columns small-centered">
    						<div class="row highlight">
    							<div class="large-50 left text-center">
    								<%=day%>
    							</div>
    							<div class="large-50 right text-center">
    								<%=hours[day]['open']%> - <%=hours[day]['close']%>
    							</div>
    						</div>
    					</div>
            		</div>
            	<% } %>
    <% } %>
        </div>
</div>

<div class="row business-extra-info margin-top-3">
	<% if(attributes.length !== 0) { var arr = ['right', 'left']; var nr = 0; %>
		<div class="row text-center">
    		<h5 class="info-title">Other information</h5>	
		</div>
    	<div class="row">       			
    	    <% for(var key in attributes) { %>
    	    	<% if(typeof(attributes[key]) !== 'object') { %>
    	        	<div class="large-6 columns highlight">
    	        			<div class="large-75 left aditional-border">
    	        				<%=key%>             				
    	        			</div>
    	        			<div class="large-25 left aditional-border">
    	        				<% if(typeof(attributes[key]) !== 'object') { nr++; %>
    	        	            	<% if(attributes[key] === true) { %>
    	        	            		Yes
    	        	            	<% } else if (attributes[key] === false) { %>
    	        	            		No
    	        	            	<% } else { %>
    	        	            		<%=attributes[key]%>
    	        	            	<% } %> 
    	        	            <% } %>
    	        			</div>
    	        	</div>
    	        <% } %>
    	    <% } %>     			
    	    <% for(var key in attributes) { %>
    	    	<% if(typeof(attributes[key]) === 'object') { nr++; %>
    	        	<div class="large-6 columns highlight <%=arr[nr%2]%>">
    	        			<div class="large-50 left aditional-border aditional-object-key text-center">
    	        				<span style="line-height: <%=Object.keys(attributes[key]).length%>em">
    	        					<%=key%>
    	        				</span>             				
    	        			</div>
    	        			<div class="large-50 left aditional-border aditional-object-value">
    	        				<% if(typeof(attributes[key]) === 'object') {
    	                    		for (var attrKey in attributes[key]) { %>
    	                    			<div class="row no-highlight">
    	                    				<div class="large-50 left aditional-border-inner">
    	                    					<%=attrKey%>
    	                    				</div>
    	                    				<div class="large-50 left aditional-border-inner">
    	                    					<% if(attributes[key][attrKey]) { %>
    	                    						Yes	
    	                    					<% } else { %>
    	                    						No	
    	                    					<% } %>
    	                    				</div>
    	                    			</div>
    	                   			<% } %>
    	                    	<% } %>
    	        			</div>
    	        	</div>
    	        <% } %>
    	    <% } %>
    	</div>
    <% } %>
</div>
