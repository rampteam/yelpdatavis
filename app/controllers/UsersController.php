<?php


class UsersController extends \BaseController {

	protected $layout = 'layouts.users';

	public function __construct() 
	{
		$this->beforeFilter('csrf', array('on' => 'post'));	
		$this->beforeFilter('yauth.app', array('except' => array('getLogin', 'postLogin', 'create', 'store')));
		$this->beforeFilter('yauth.setinfo', array('only' => array('getLogin', 'postLogin', 'create', 'store')));
		$this->beforeFilter('yauth.dennied', array('only' => array('create', 'store')));
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		$this->layout->content = View::make('users.login');
	}

	public function postLogin()
	{
		// get input data
		$credentials = Input::all();
		$credentials = array_only($credentials, array('email', 'password'));

		//validation
		$validation = new Yelp\Validators\Login($credentials);
		if ($validation->passes())
		{
			$user = new Yelp\Auth\User\DatabaseUser($credentials);

			if($user->authenticate()) 
			{
				$data =	YAuth::login($user);
				Session::put('token', $data['token']);
				return Redirect::to('/');
			}
		}
		return Redirect::back()->withErrors('Invalid Email or Password');
	}

	public function getLogout()
	{
		YAuth::logout();
		return Redirect::action('UsersController@getLogin');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validation = new Yelp\Validators\Register;

		if($validation->passes())
		{
			
			$userdata = array_only(Input::all(), array('email', 'firstName', 'lastName'));
			$user = new User($userdata);
			$user->password =  Hash::make(Input::get('password1'));
			$user->save();
			if(!$user)
			{
				App::abort(500, 'Internal server error');
			}
			return Redirect::action("UsersController@getLogin");
		}
		return Redirect::back()->withInput()->withErrors($validation->errors);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$id = intval($id);
		$authenticatedUserId = YAuth::user()['id'];

		if($id !== $authenticatedUserId)
		{
			App::abort(403);
		}
		$user = User::find($id);
		$this->layout->content = View::make('users.show',array('user'=> $user));
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$id = intval($id);
		$authenticatedUserId = YAuth::user()['id'];

		if($id !== $authenticatedUserId)
		{
			App::abort(403);
		}
		$user = User::find($id);
		$this->layout->content = View::make('users.edit',array('user'=> $user));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($userId)
	{
		$id = YAuth::user()['id'];
		$validation = new Yelp\Validators\Edit; 

		if($validation -> passes())
		{

			if($id != $userId)
			{
				App::abort(403, 'Access denied');
			}
			$pass = Input::get('password', ''); 
			$user = User::find($id);  

			if(!empty($pass))
			{
				if(!Hash::check($pass, $user->getAuthPassword()))
				{
					return Redirect::back()->withInput()->withErrors(array('password' => 'Invalid Password'));
				}
				$user ->password =  Hash::make(Input::get('newpassword1'));
			}
			$user ->firstName = Input::get('firstName');
			$user ->lastName = Input::get('lastName');
			
			$user ->save();
			return Redirect::to('/');
			
		}
		return Redirect::back()->withInput()->withErrors($validation->errors);
	}
	
}