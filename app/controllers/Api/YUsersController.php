<?php

namespace Api;

use Yelp\Model\YUser;
use Yelp\Processing\User\FriendsGraph;
use Yelp\Processing\User\ReviewStars;
use Yelp\Processing\User\StarsOverview;
use Yelp\Processing\User\Businesses;
use Yelp\Processing\Collection\MonthlyReviews;

class YUsersController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get client options
        $options = $this->getInputParams(array('filter', 'limit', 'offset', 'orderBy', 'columns', 'search', 'withFriends', 'friendsColumns'));
        // create query
        $query = YUser::query();
        
        // apply filter options
        YUser::applyFilter($query, $options['filter']);
        
        // apply search keywordss
        YUser::applySearch($query, $options['search'], array('name'));
        
        // set order by
        YUser::applyOrder($query, $options['orderBy']);
        
        // apply limit and offset
        YUser::applyLimit($query, $options['limit']);
        YUser::applyOffset($query, $options['offset']);
        
        $columns = is_array($options['columns']) ? $options['columns'] : array();
        // execute query
        // 
        $users = $query->get();

        if(empty($users) || !is_array($users))
        {
            return $users;
        }

        if($options['withFriends'])
        {
            $this->loadFriends($users, $options['friendsColumns']);
        }

        $users = $this->extractColumns($users, $options["columns"]);

        YUser::convertMongoDateToText($users, array('yelping_since'));

        //dump(\DB::connection('mongodb')->getQueryLog());
        return \Response::make($users, \HttpStatus::STATUS_OK);
    }

    protected function loadFriends(array &$users, $columns)
    {
        //parse each user gather all friends 
        $friendsIds = array();
        foreach ($users as $userData) 
        {   
           $friendsIds = array_merge($friendsIds, $userData['friends']);
        }

        $friendsIds  = array_unique($friendsIds);

        if(empty($friendsIds))
        {
            return;
        }

        $query = YUser::query();
        
        // apply filter options
        YUser::applyFilter($query, array("user_id" => $friendsIds));

        if(!empty($columns))
        {
            $columns = array_merge($columns, array("user_id"));

            $friends = $query->get($columns);
        }else
        {
            $friends = $query->get();
        }


        if(empty($friends))
        {
            return;
        }

        //make assoc

        $friendsAssoc = \YUtil::makeAssocByKey($friends, 'user_id');

        foreach ($users as &$userData) 
        {   
            $friends = array();
            foreach ($userData["friends"] as $fId) 
            {
                $friend  = array_get($friendsAssoc, $fId, null);

                if(empty($friend)) continue;
            
                $friends[]  = $friend;
            }

            $userData["friends"] = $friends;
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        
        $user = $this->getUser($id);

        if (empty($user)) {
           return \Response::make(array(
            'Message' => 'User not found',
            ), \HttpStatus::ERROR_NOT_FOUND);
       }


       return \Response::make($user, \HttpStatus::STATUS_OK);
   }

   protected function getUser($id, $options = array())
   {    
        $options = \YUtil::getOrSetDataKeys(array("columns"), $options);

        $query = YUser::query();

        $query->where('user_id', $id);

        $user = YUser::getFirstRow($query, $options["columns"]);

        if(empty($user)) return $user;

        YUser::convertMongoDateFieldsToText($user, array('yelping_since'));

        return $user;
   }

   public function getUserFriendsFor($userId){
        try {

            $friendsGraph = new FriendsGraph();

            $users = $friendsGraph->getFriendsGraphDataFor($userId);

            if($users === null || !is_array($users))
            {
                return \Response::make($users, \HttpStatus::ERROR_SERVER);
            }
           
            return \Response::make($users, \HttpStatus::STATUS_OK);

        } catch (\Exception $e) {
           
          return $this->getExceptionResponse($e);
        }
   }

   public function getUserFriends()
   {   
        try {

            $queryData =  $this->getInputParams(array('filter', 'limit', 'offset' ,'columns', 'search', 'orderBy'));
            
            $friendsGraph = new FriendsGraph($queryData);

            $users = $friendsGraph->getUsers();

            if($users === null || !is_array($users))
            {
                return \Response::make($users, \HttpStatus::ERROR_SERVER);
            }
           
            return \Response::make($users, \HttpStatus::STATUS_OK);

        } catch (\Exception $e) {
           
          return $this->getExceptionResponse($e);
        }
    }

    public function getUserReviewStats()
    {   
        try {

            $userId = \Request::query('user_id', null);
                
            if(empty($userId))
            {
                throw new \InvalidArgumentException("Please an pass user id!");
            }

            $starsGen = new ReviewStars($userId);

            $stars = $starsGen->getReviewStars();


            if($stars === null || !is_array($stars))
            {
                return \Response::make($stars, \HttpStatus::ERROR_SERVER);
            }
           
            return \Response::make($stars, \HttpStatus::STATUS_OK);

        } catch (\Exception $e) {
           
            return $this->getExceptionResponse($e);
        }
    }

     public function getUserStars()
    {   
        try {
            
            $userId = \Request::query('user_id', null);
                
            if(empty($userId))
            {
                throw new \InvalidArgumentException("Please an pass user id!");
            }

            $userReviews = new StarsOverview();

            $reviews = $userReviews->getStarsOverview($userId);


            if($reviews === null || !is_array($reviews))
            {
                return \Response::make($reviews, \HttpStatus::ERROR_SERVER);
            }
           
            return \Response::make($reviews, \HttpStatus::STATUS_OK);

        } catch (\Exception $e) {
           
            return $this->getExceptionResponse($e);
        }
    }


    public function getUserBusinesses()
    {   
        try {
            
            $userId = \Request::query('user_id', null);
                
            if(empty($userId))
            {
                throw new \InvalidArgumentException("Please an pass user id!");
            }

            $userBusinessData = new Businesses($userId);

            $userBusinessData = $userBusinessData->getUserBusinessData();

            if($userBusinessData === null || !is_array($userBusinessData))
            {
                return \Response::make($userBusinessData, \HttpStatus::ERROR_SERVER);
            }
           
            return \Response::make($userBusinessData, \HttpStatus::STATUS_OK);

        } catch (\Exception $e) {
           
            return $this->getExceptionResponse($e);
        }
    }

    public function getTimeFrameReviews($params = array())
    {      

        $options = $this->getInputParams(array('user_id', 'last', 'timeFrame'));
        $options['entityId'] = 'user_id';

        if(empty($options['user_id']))
        {
            throw new \InvalidArgumentException("Please specify a user_id!");
        }

        $p = new MonthlyReviews($options);
        return $p->getData($options);
    }


}