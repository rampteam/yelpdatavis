<?php

namespace Api;

use Yelp\Support\Util;

class BaseController extends \Controller{
	
   
    /** ?limit=10&orderBy[city]=ASC&columns[]=full_address&search=sth&offset=10
     * Returns API query string options
     * @param array $specificOptions
     */
    protected function getInputParams(array $options)
    {   
    	$userOptions = array();
    
    	foreach ($options as $key){
    	    
    		$option = \XssInput::get($key, null);
    		
            $value = $this->getInputValue($option);

    		$userOptions[$key] = $value;
    	}
    	
    	return $userOptions;
    }


    protected function getInputValue($value)
    {   
       if(is_array($value))
       {
            foreach ($value as $key => $val) 
            {
                $value[$key] = $this->treatValue($val);
            }
            
       }else
       {
            $value = $this->treatValue($value);
       }

       return $value;
    }

    protected  function treatValue($value)
    {
        if(empty($value))
        {
            return null;
        }

        if(is_string($value))
        {
            $value = str_replace('&amp;', '&', $value);
        }

        return $value;
    }


    protected function getBasicRelations(array $options)
    {
        $relations = array();
            // join reviews

        foreach ($options as $key => $value) {
            if(!empty( $value) && strpos($key, "with") === 0)
            {
                $relname = trim(strtolower(substr($key, 4)));
                if(!empty($relname));
                $relations[] = $relname;
            }
        }

        return $relations;
    }

    protected function extractColumns($data, $inputData)
    {   
        return Util::extractColumns($data, $inputData);
    }

    protected function getExceptionResponse(\Exception $e)
    {   
        $code = 500;
        if($e instanceof InvalidArgumentException)
        {
            $code = \HttpStatus::ERROR_VALIDATION_FAILURE;
        }
        throw $e;
        return \Response::make(array(
            'Message' => $e->getMessage(),
        ), $code);

    }

}