<?php

namespace Api;

use Yelp\Model\YBusiness;
use Yelp\Processing\Business\StatesCities;
use Yelp\Processing\Business\GeoLocation;
use Yelp\Processing\Business\Treemap;
use Yelp\Processing\Collection\MonthlyReviews;

class YBusinessesController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get client options
        $options = $this->getInputParams(array('filter', 'limit', 'offset', 'orderBy', 'columns', 'search'));

        // create query
        $query = YBusiness::query();
        
        // apply filter options
        YBusiness::applyFilter($query, $options['filter']);

        // apply search keywords
        YBusiness::applySearch($query, $options['search'], array('name'));
        
        // set order by
        YBusiness::applyOrder($query, $options['orderBy']);
        
        // apply limit and offset
        YBusiness::applyLimit($query, $options['limit']);
        YBusiness::applyOffset($query, $options['offset']);
        
        $columns = is_array($options['columns']) ? $options['columns'] : array();
        // execute query
        $businesses = $query->get($columns);
        //dump(\DB::connection('mongodb')->getQueryLog());
         
        return \Response::make($businesses, \HttpStatus::STATUS_OK);
    }

    public function getTreemap(){
        // get client options
        $options = $this->getInputParams(array('filter', 'limit', 'offset', 'orderBy', 'columns', 'search'));

        $treemap = new Treemap($options);

        return \Response::make($treemap->getData(), \HttpStatus::STATUS_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {   
        $userParams = array('withReviews','withCheckIns', 'withTips');
       // parse client data
        $options = $this->getInputParams($userParams);

        $business  = YBusiness::with($this->getBasicRelations($options))->where('business_id', $id)->first();

        if (empty($business)) {
             return \Response::make(array(
                'Message' => 'Business not found',
            ), \HttpStatus::ERROR_NOT_FOUND);
        }

        return \Response::make($business, \HttpStatus::STATUS_OK);
    }


    public function getStates($params = array())
    {   

        $options = $this->getInputParams(array('filter', 'excludeFilter'));

        $p = new StatesCities();
        return $p->getStatesAndCities($options);
    }

    public function getTimeFrameReviews($params = array())
    {      

        $options = $this->getInputParams(array('business_id', 'last', 'timeFrame'));
        $options['entityId'] = 'business_id';

        if(empty($options['business_id']))
        {
            throw new \InvalidArgumentException("Please specify a business id!");
        }

        $p = new MonthlyReviews($options);
        return $p->getData($options);
    }


    public function getGeo()
    {   
        //"coord":[-111.964485,33.383123]
        $options = $this->getInputParams(array('filter', 'longitude', 'type' ,'latitude', 'maxDistance', 'distanceMultiplier', 'columns', 'limit'));

        $p = new GeoLocation($options);
        return $p->getData($options);
    }


}