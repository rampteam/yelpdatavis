<?php

namespace Api;

use Yelp\Model\YReview;
use Yelp\Model\YUser;
use Yelp\Model\YBusiness;
use Yelp\Processing\Review\Review as ReviewAPI;

class YReviewsController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {   
       // get client options
        $options = $this->getInputParams(array('filter', 'limit', 'offset', 'orderBy', 'columns', 'search', 'withUser', 'withBusiness'));
        // create query
        $review = new ReviewAPI($options);

        $reviews = $review->getReviews();

        return \Response::make($reviews, \HttpStatus::STATUS_OK);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        // parse client data
        $options = $this->getInputParams(array('withUser'));
        
        $relations = array();
        
        if(!is_null($options['withUser'])){
            $relations[] = 'user';
        }

        $review  = YReview::with($relations)->where('_id', $id)->first();
    
        $arrReview = $review->toArray();        

        if (empty($arrReview)) {
             return \Response::make(array(
                'Message' => 'Review not found',
            ), \HttpStatus::ERROR_NOT_FOUND);
        }

        YReview::convertMongoDateFieldsToText($arrReview, array('date'));

        return \Response::make($arrReview, \HttpStatus::STATUS_OK);
    }
}