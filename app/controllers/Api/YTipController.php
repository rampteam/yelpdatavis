<?php

namespace Api;

use Yelp\Model\YTip;

class YTipController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
         // get client options
        $options = $this->getInputParams(array('filter', 'limit', 'offset', 'orderBy', 'columns', 'search'));
        // create query
        $query = YTip::query();
        
        // apply filter options
        YTip::applyFilter($query, $options['filter']);
        
          // apply search keywords
        YTip::applyTextSearch($query, $options['search'], 'text');

        // set order by
        YTip::applyOrder($query, $options['orderBy']);
        
        // apply limit and offset
        YTip::applyLimit($query, $options['limit']);
        YTip::applyOffset($query, $options['offset']);
        
        $columns = is_array($options['columns']) ? $options['columns'] : array();
        // execute query
        $reviews = $query->get($columns);
        
        //dump(\DB::connection('mongodb')->getQueryLog());
        return \Response::make($reviews, \HttpStatus::STATUS_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $userParams = array('withBusiness','withUser');
        // parse client data
        $options = $this->getInputParams($userParams);

        $tip  = YBusiness::with($this->getBasicRelations($options))->where('_id', $id)->first();

        if (empty($tip)) {
             return \Response::make(array(
                'Message' => 'Business not found',
            ), \HttpStatus::ERROR_NOT_FOUND);
        }

        return \Response::make($tip, \HttpStatus::STATUS_OK);
    }


}