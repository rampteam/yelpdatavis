<?php namespace Yelp\TokenManager;

use Illuminate\Support\ServiceProvider;


class TokenManagerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindShared('token-manager', function ($app)
        {
            return new TokenManager();
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('token-manager');
    }

}