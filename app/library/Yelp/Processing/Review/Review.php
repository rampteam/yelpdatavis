<?php 

namespace Yelp\Processing\Review;
use Yelp\Model\YUser;
use Yelp\Model\YBusiness;
use Yelp\Model\YReview;
use Yelp\Processing\Base;

class Review extends Base
{
	protected $requestData = array();

	protected $links = array();
	
	public function __construct($requestData = array())
	{
		$this->requestData = $requestData;
	}

	protected function getFromBusiness()
	{
		return $this->getReviewsFrom("Yelp\Model\YBusiness", "business_id");

	}

	protected function getFromUser()
	{
		return $this->getReviewsFrom("Yelp\Model\YUser", "user_id");

	}

	public function getReviews()
	{	
		$reviews = array();

		$options = $this->requestData;
		$businessIds =  $this->getFromBusiness();
		$userIds =  $this->getFromUser();
		
		if(empty($businessIds) || empty($userIds))
		{
			return $reviews;
		}

		$filters = YReview::getValidFiltersOnly($options['filter']);

		$this->buildFilters($filters, $businessIds, "business_id");
		$this->buildFilters($filters, $userIds, "user_id");

		$query = YReview::query();
   
        // apply filter options
        YReview::applyFilter($query, $filters);
        
        // apply search keywords
        YReview::applyTextSearch($query, $options['search'], 'text');
        
        // set order by
        YReview::applyOrder($query, $options['orderBy']);
        
        // apply limit and offset
        YReview::applyLimit($query, $options['limit']);
        YReview::applyOffset($query, $options['offset']);
        
        $columns = is_array($options['columns']) ? $options['columns'] : array();

        // execute query
        $reviews = $query->get();

        //dump(\DB::connection('mongodb')->getQueryLog());
        if($options["withUser"])
        {   
            $reviews = YReview::loadBelongsTo($reviews, 'user_id', 'user_id', 'Yelp\Model\YUser', "user");
        }

        if($options["withBusiness"])
        {   
            $reviews = YReview::loadBelongsTo($reviews, 'business_id', 'business_id', 'Yelp\Model\YBusiness', "business");
        }

        $reviews = \YUtil::extractColumns($reviews, $columns);

        YUser::convertMongoDateToText($reviews, array('date'));		

		return $reviews;
	}

	protected function buildFilters(&$filter, $ids, $type)
	{	
		if(empty($ids)) return ;

		if($ids === true) return;

		$filter[$type] = $ids;

	}

	protected function getReviewsFrom($class, $idKey)
	{	
		$options = $this->requestData;

		$dataFilter = $class::getValidFiltersOnly($options['filter']);

		if(empty($dataFilter)) return true;
			
		$query = $class::query();

        $class::applyFilter($query, $dataFilter);
       	
       	$rows = $query->get(array($idKey));

        $ids = array_pluck($rows, $idKey);

        //dump(\DB::connection('mongodb')->getQueryLog());
        if(!is_array($ids))
        {
        	return $ids;
        }

        $ids = array_unique($ids);

        return $ids;
	}

	
}