<?php 


namespace Yelp\Processing\Business;
use Yelp\Model\YBusiness;
use Yelp\Processing\Base;

class StatesCities extends Base
{
	protected $stateToCity = [
	"FIF" => ['long_name' => 'Fife', 'country' => 'United Kingdom'],
	"ELN" => ['long_name' => 'East Lothian', 'country' => 'United Kingdom'],
	"MLN" => ['long_name' => 'Midlothian', 'country' => 'United Kingdom'],
	"ON" => ['long_name' => 'London', 'country' => 'United Kingdom'],
	"NTH" => ['long_name' => 'Northamptonshire', 'country' => 'United Kingdom'],
	"KHL" => ['long_name' => 'Northamptonshire', 'country' => 'United Kingdom'],
	"XGL" => ['long_name' => 'Northamptonshire', 'country' => 'United Kingdom'],
	"SCB" => ['long_name' => 'Midlothian', 'country' => 'United Kingdom'],
	"EDH" => ['long_name' => 'Edinburgh', 'country' => 'United Kingdom'],
	];

	protected $inputOptions = array();
	
	public function __construct($inputOptions = array())
	{
		$this->inputOptions = $inputOptions;
	}

	public function getStatesAndCities($inputOptions = array())
	{
		$agg = $this->getAggBuilder();

		$p = array();

		$groupId = $agg->getCollMap(array("state")); 

		$groupBody = array(
			"cities" => array('$addToSet' => '$city')
			);

		$agg->bindGroup($p, $groupId, $groupBody);

		$params = array_merge(YBusiness::getDBColl() ,array('pipeline' => $p));

		$data = $agg->exec(
			$params
			);

		$agg->flatIds($data);
		

		$tmpCountries = array();
		foreach ($data as $stateData) 
		{
			$country = 'United States';
			if(array_key_exists($stateData['state'], $this->stateToCity))
			{
				$country = $this->stateToCity[$stateData['state']]['country'];
				$stateData['state'] = $this->stateToCity[$stateData['state']]['long_name'];
			}

			if(!isset($tmpCountries[$country]))
			{
				$tmpCountries[$country] = [];
			}


			$state = array();
			$state["short_name"] = $state["long_name"]  = ucwords($stateData["state"]);
			$state["cities"] = array();

			foreach ($stateData["cities"] as $cityName) 
			{
				$state["cities"][] = array("name" => $cityName);
			}

			$tmpCountries[$country][$state["long_name"]] = $state;
		}
		//change to array
		$countries = [];
		foreach ($tmpCountries as $country => $states) {
			$countries[] = [
				'name' => $country,
				'states' => $states,
			];
		}

		return $countries;
		
	}


	protected function getStateAbbreviations()
	{

		return array(
			"Alabama"       => "AL",
			"Alaska"        => "AK",
			"Arizona"       => "AZ",
			"Arkansas"      => "AR",
			"California"    => "CA",
			"Colorado"      => "CO",
			"Connecticut"   => "CT",
			"Delaware"      => "DE",
			"Florida"       => "FL",
			"Georgia"       => "GA",
			"Hawaii"        => "HI",
			"Idaho"         => "ID",
			"Illinois"      => "IL",
			"Indiana"       => "IN",
			"Iowa"          => "IA",
			"Kansas"        => "KS",
			"Kentucky"      => "KY",
			"Louisiana"     => "LA",
			"Maine"         => "ME",
			"Maryland"      => "MD",
			"Massachusetts" => "MA",
			"Michigan"      => "MI",
			"Minnesota"     => "MN",
			"Mississippi"   => "MS",
			"Missouri"      => "MO",
			"Montana"       => "MT",
			"Nebraska"      => "NE",
			"Nevada"        => "NV",
			"New Hampshire" => "NH",
			"New Jersey"    => "NJ",
			"New Mexico"    => "NM",
			"New York"      => "NY",
			"North Carolina"=> "NC",
			"North Dakota"      => "ND",
			"Ohio"      => "OH",
			"Oklahoma"      => "OK",
			"Oregon"        => "OR",
			"Pennsylvania"      => "PA",
			"Rhode Island"      => "RI",
			"South Carolina"        => "SC",
			"South Dakota"      => "SD",
			"Tennessee"     => "TN",
			"Texas"     => "TX",
			"Utah"      => "UT",
			"Vermont"       => "VT",
			"Virginia"      => "VA",
			"Washington"        => "WA",
			"West Virginia"     => "WV",
			"Wisconsin"     => "WI",
			"Wyoming"       => "WY"
			);

	}//

	protected function getAbbStates()
	{
		return array_flip($this->getStateAbbreviations());
	}
}