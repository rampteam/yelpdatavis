<?php 

namespace Yelp\Processing\Business;
use Yelp\Model\YBusiness;

use  Yelp\Processing\Base;

/**
* 
*/
class Treemap extends Base
{
    protected $options;

    protected $data = array();

    function __construct(array $options)
    {
      $this->options = $options;
    }


    public function getData(){

        $options = $this->options;

        // create query
        $query = YBusiness::query();
        
        // apply filter options
        YBusiness::applyFilter($query, $options['filter']);

        // apply search keywords
        YBusiness::applySearch($query, $options['search'], array('name'));
        
        // set order by
        YBusiness::applyOrder($query, $options['orderBy']);
        
        // apply limit and offset
        YBusiness::applyLimit($query, $options['limit']);
        YBusiness::applyOffset($query, $options['offset']);
        
        $columns = is_array($options['columns']) ? $options['columns'] : array();
        // execute query
        $businesses = $query->get($columns);

        $geo = $this->extractCities($businesses);

        $data = $this->getTreeMapData($geo);

        return $this->getTreemapTemplate($data);
        
    }

    protected function getTreemapTemplate($data){
    /*    return array(
            "name" => "businesses",
            "children" => empty($data) ? array() : $data
        );*/

        return $data;
    }

    protected function extractCities($businesses){
        if(empty($businesses)){
            return ;
        }

        $classifier = array();

        foreach ($businesses as $business) {
            $path = $this->getDotPath($business["state"], $business["city"]);

            array_set($classifier, $path, array(
                "name" => $business["city"],
                "value" => 1
            ));           
        }
        
        return $classifier;
    }

    protected function getTreeMapData($geo){

        if(empty($geo)){
            return;
        }
        
        $data = array();

        foreach ($geo as $stateName => $cities) {
            $data[] = array(
                "name" => $stateName,
                "children" => array_values($cities)
            );    
        }

        return $data;
    }

    protected function getDotPath(){
        $params = func_get_args();
        if(is_array($params[0])){
            $params = $params[0];
        }

        return implode(".", $params);
    }
}