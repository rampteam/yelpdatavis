<?php 

namespace Yelp\Processing\Collection;
use Yelp\Model\YBusiness;
use Yelp\Model\YReview;
use  Yelp\Processing\Base;

class MonthlyReviews extends Base
{

    protected $entityId = array();
	protected $entityIdField = null;

	protected $options = array();
	
	public function __construct($options = array())
	{	
        $this->entityIdField = array_get($options, 'entityId', null);
		$this->validateData($options);
        $this->entityId = array_get($this->options, $this->entityIdField); 
        if(empty($this->entityId))
        {
            throw new \InvalidArgumentException("Please specify an entity id!");
        }
	}

	protected function validateData($options)
	{	
		$allowedTimeFrame = array("daily", "monthly", "yearly");
		$defaults = array(
			"timeFrame" => "monthly",
			"last"		=> 6,
            $this->entityIdField => ""
		);

		$this->options = \YUtil::getOrSetDataKeys($defaults, $options);

        foreach ($defaults as $key => $value) 
        {
            $val = array_get($options, $key, null);
            $this->options[$key] = $value;

            if(!empty($val))
            {
                $this->options[$key] = $val;
            }
        }

		if(!in_array($this->options["timeFrame"], $allowedTimeFrame))
		{
			throw new \InvalidArgumentException("Invalid timeFrame requested!");
		}

        if($this->entityIdField == null)
        {      
            throw new \InvalidArgumentException("No entity id field passed!");
        }
	}

	public function getData()
	{
		$agg = $this->getAggBuilder();

        $p = array();

        //match pipe first
        $filters = array(
        	$this->entityIdField => array($this->entityId)
        );

        $params = array(
        	"filters" => $filters
        );

        $agg->bindMatch($p, $params);

        $timeFrame = $this->options["timeFrame"];

     
        $timeFrameFields = $agg->getFieldsByTimeFrame( $timeFrame);

        //get fields 
        $tGid = $agg->getTimeFrameGroupId("date", $timeFrame);
        $bGroupId = $agg->getCollMap(array($this->entityIdField )); 
        $groupId = array_merge($tGid, $bGroupId);

        $groupBody = array(
            "nr_reviews" => array('$sum' => 1),
            "avg_stars" => array('$avg' => '$stars')
        );

        $agg->bindGroup($p, $groupId, $groupBody);

        //sort 
        $sortField = $this->getTimeFrameSortField($timeFrameFields);
        $agg->bindSort($p,  array('_id.'.$sortField => -1));

        //bind limit
        //
        $agg->bindLimit($p, $this->options["last"]);

        //ump($p);

        $params = array_merge(YReview::getDBColl() ,array('pipeline' => $p));

        $data = $agg->exec(
            $params
        );	

        $agg->flatIds($data);

        return $data;
	}

	protected function getTimeFrameSortField(array $indices)
	{
		return array_pop($indices);
	}

}