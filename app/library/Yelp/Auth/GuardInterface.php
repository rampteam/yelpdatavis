<?php namespace Yelp\Auth;

use Yelp\Auth\User\YUserInterface;

interface GuardInterface
{
    /**
     * check if current user is authenticated
     *
     * @return boolean
     */
    public function check();

    /**
     * @param YUserInterface $user
     * @return mixed
     */
    public function login(YUserInterface $user);

    /**
     * Logout current user
     *
     * @return
     */
    public function logout();
} 