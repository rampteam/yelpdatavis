<?php namespace Yelp\Auth;

use Yelp\Auth\User\YUserInterface;
use ArrayObject;
use Yelp\TokenManager\Facades\TokenManager;


class Guard implements GuardInterface
{
    /**
     * current user token
     * @var string
     */
    protected $token = null;

    /**
     * current user
     *
     * @var array
     */
    protected $user;

    /**
     * flag, show if logout has been called
     *
     * @var bool
     */
    protected $logout = false;


    /**
     * check if current user is authenticated
     *
     * @return boolean
     */
    public function check()
    {
        return $this->user() !== null;
    }

    /**
     * @param YUserInterface $user
     * @return mixed
     */
    public function login(YUserInterface $user)
    {
        if (!$user->check())
            return false;

        $this->token = TokenManager::createToken($user->getPersistentData());
        $this->setUser($user->getPersistentData());
        return array('token' => $this->token);
    }

    /**
     * Logout current user
     *
     * @return void
     */
    public function logout()
    {
        if($this->token)
        {
            TokenManager::delete($this->token);
            $this->user = null;
            $this->logout = true;
        }
    }

    /**
     * get user token
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * get current user
     * @return array|bool
     */
    public function user()
    {
        if ($this->logout)
            return false;
        return $this->user;
    }

    public function setUser(array $user)
    {
        $this->user = $user;
        $this->logout = false;
    }

    public function token($token)
    {
    	$data = TokenManager::getData($token);

    	if($data === false)
    	{
    		return $data;
    	}

    	$this->user = $data;

    	$this->token = $token;

    	return $token;
    }
}