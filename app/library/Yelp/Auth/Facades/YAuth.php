<?php namespace Yelp\Auth\Facades;

use Illuminate\Support\Facades\Facade;


class YAuth extends Facade {
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'yauth';
    }
}