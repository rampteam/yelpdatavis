<?php namespace Yelp\Validators;

/**
 * Register user validators
 **/
class Register extends Validator
{
	public static $rules = array(
		'firstName' => array('required', 'alpha'),
		'lastName' => array('required', 'alpha'),
		'email' => array('required', 'email', 'unique:users'),
		'password1'  => array('required', 'min:6'),
		'password2'  => array('required', 'min:6', 'same:password1'),
		'terms' => 'required',
	);
}