<?php 

namespace Yelp\Support;

class HttpStatus
{
	const ERROR_VALIDATION_FAILURE = 422;
    const ERROR_AUTHORIZATION_FAILURE = 403;
    const ERROR_NOT_FOUND = 404;
    const ERROR_BAD_REQUEST = 400;
    const STATUS_OK = 200;
    const STATUS_NO_CONTENT = 204;
    const STATUS_CREATED = 201;
    const ERROR_SERVER = 500;
    const ERROR_CONFLICT = 409;
}