<?php 

if ( ! function_exists('dump'))
{
    function dump()
    {
        list($callee) = debug_backtrace();
        $arguments = func_get_args();
        $total_arguments = count($arguments);
    
        echo '<fieldset style="background: #fefefe !important; border:2px red solid; padding:5px">';
        echo '<legend style="background:lightgrey; padding:5px;">'.@$callee['file'].' @ line: '.@$callee['line'].'</legend><pre>';
        $i = 0;
        foreach ($arguments as $argument)
        {
            echo '<br/><strong>Debug #'.(++$i).' of '.$total_arguments.'</strong>: ';
            var_dump($argument);
        }
    
        echo "</pre>";
        echo "</fieldset>";
        die(0);
    }

}


if ( ! function_exists('elog'))
{
    function elog()
    {

        $arguments = func_get_args();
        $total_arguments = count($arguments);
        echo "<pre>";
        foreach ($arguments as $argument)
        {
            var_dump($argument);
        }
    
        echo "</pre>";
        die(0);
    }
}

if ( ! function_exists('startsWith'))
{
 
    function startsWith($haystack, $needle)
    {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }
}

if ( ! function_exists('endsWith'))
{
 
    function endsWith($haystack, $needle)
    {
        return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
    }
}


