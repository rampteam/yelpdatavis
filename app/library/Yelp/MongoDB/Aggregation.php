<?php 

namespace Yelp\MongoDB;

use MongoClient;

class Aggregation 
{
	
	protected $connection = null;

	function __construct(MongoClient $conn)
	{
		$this->connection = $conn;
	}	


	public function applyFilters($filters = array(), $excludeFilters = array())
	{	
		$query = array();

		if (!empty($filters) && is_array($filters))
		{
			foreach ($filters as $filter => $values)
			{
				$query[$filter]['$in'] = $values;
			}
		}

		if (!empty($excludeFilters) && is_array($excludeFilters))
		{
			foreach ($excludeFilters as $filter => $values)
			{
				$query[$filter]['$nin'] = $values;
			}
		}

		return $query;
	}

	public function getCollMap($dimensions = array())
	{
		$groupId = array();
		if(empty($dimensions)) return $groupId;

		foreach ($dimensions as $field)
		{	
			$groupId[$field] = '$' . $field;
		}

		return $groupId;
	}

	public function bindMatch(&$pipe, $params = array())
	{
		$pipe[] = $this->getMatchPipe($params);
	}

	public function bindRawMatch(&$pipe, $query = array())
	{
		$pipe[] = $this->getRawMatchPipe($query);
	}

	public function bindGroup(&$pipe, $groupId = array(), $groupBody = array())
	{
		$pipe[] = $this->getGroupPipe($groupId, $groupBody);
	}

	public function getMatchPipe($params = array())
	{	
		$defaults = array(
			'query'          => array(),
			'filters'        => array(),
			'excludeFilters' => array()
		);	
		extract($this->getDataKeys($defaults, $params));

		$filters = $this->applyFilters($filters, $excludeFilters);
		
		$match = array_merge($query, $filters);
		
		return array('$match' => $match);
	}

	public function getRawMatchPipe($query = array())
	{
		return array('$match' => $query);
	}

	public function bindUnwind(&$pipe, $field) 
	{
		$pipe[] = $this->getRawUnwindPipe($field);
	}

	public function getRawUnwindPipe($field)
	{
		return array('$unwind' => $field);
	}

	public function bindLimit(&$pipe, $limit) 
	{
		$raw = $this->getRawLimitPipe($limit);

		if($raw !== null)
		{
			$pipe[] = $raw;
		}
	}

	public function getRawLimitPipe($limit)
	{	

		if(!empty($limit) && is_numeric($limit))
        {	
        	$limit = intval($limit);
        	$cLimit = YBaseQuery::getBoundLimit($limit);
            return array('$limit' => $cLimit);
        }

		return null;
	}

	public function bindOffset(&$pipe, $offset) 
	{
		$raw = $this->getRawOffsetPipe($offset);

		if($raw !== null)
		{
			$pipe[] = $raw;
		}
	}

	public function getRawOffsetPipe($offset)
	{	

		if(!empty($offset) && is_numeric($offset))
        {	
        	$offset = intval($offset);
            return array('$skip' => $offset);
        }

		return null;
	}

	public function bindSort(&$pipe, $sortData) 
	{
		$raw = $this->getRawSortAssocPipe($sortData);

		if($raw !== null)
		{
			$pipe[] = $raw;
		}
	}

	public function getRawSortAssocPipe(array $sortData = array())
	{	
		$columns = array();

		$strVals = array('asc' => 1, 'desc' => -1);

		foreach ($sortData as $key => $value) 
		{
			if(is_string($value))
			{
				$value = strtolower($value);
				if(isset($strVals[$value]))
				{
					$columns[$key] = $strVals[$value];
				}
			}elseif(is_numeric($value))
			{
				$value = intval($value);
				if($value=== 1 || $value === -1)
				{
					$columns[$key] = $value;
				}
			}
		}

		if(!empty($columns))
		{
			return array('$sort' => $columns);
		}

		return null;
	}

	public function getRawProjectPipe($project = array())
	{
		return array('$project' => $project);
	}

	public function bindRawProject(&$pipe, $project = array())
	{
		$pipe[] = $this->getRawProjectPipe($project);
	}

	public function getGroupPipe($groupId = array(), $groupBody = array())
	{
		$groupBody["_id"] = $groupId;
		return  array( '$group' => $groupBody);
	}

	public function exec($params = array())
	{	
		
		$db = $this->connection->selectDB($params['dbName']);

		if($db)
		{
			$cmd = array(
				'aggregate' => $params['collName'],
				'pipeline' => $params['pipeline'],
				);

			$results = $db->command($cmd, array('socketTimeoutMS' => -1));
			if(empty($results["ok"]))
			{
				throw new \Exception("MongoDB aggregation failed: Msg - {$results['errmsg']} Code - {$results['code']}");
			}

			return array_get($results, "result", null);
		}
	}

	protected function getDataKeys($defaults = array(), $from)
	{
		foreach ($defaults as $key => &$data) 
		{
			$data = array_get($from, $key, $data);
		}

		return $defaults;
	}

	public function flatIds(&$results)
	{	
		if(empty($results) && !is_array($results)) return $results;

 		foreach ($results as &$result) 
		{
			if(isset($result["_id"]) && is_array($result["_id"]))
			{
				$result = array_merge($result, $result["_id"]);
				unset($result["_id"]);
			}
		}
	}

	public function getTimeFrameGroupId($timestampField, $timeFrame)
	{
		$groupId = array();

		$dateAgg = $this->getAggregatesByInterval($timeFrame);

		foreach($dateAgg as $groupByField)
		{
			switch($groupByField)
			{
				case "hour" :
				$groupId["h"] = array('$' . $groupByField => '$'.$timestampField);
				break;
				case "dayOfMonth" :
				$groupId["d"] = array('$' . $groupByField => '$'.$timestampField);
				break;
				case "month" :
				$groupId["m"] = array('$' . $groupByField => '$'.$timestampField);
				break;
				case "year" : 
				$groupId["y"] = array('$' . $groupByField => '$'.$timestampField);
				break;
				default :
				//do nothing
			}	    	    
		}

		return $groupId;
	}


	public function getAggregatesByInterval($interval)
	{
		$result = array();

		switch (strtolower($interval))
		{	
			case "hourly":
			$result = array('year', 'month', 'dayOfMonth', 'hour');
			break;
			case "daily" : 
			$result =  array('year', 'month', 'dayOfMonth');
			break;
			case "monthly":
			$result = array('year', 'month');
			break;
			case "yearly":
			$result = array('year');
			break;
			default:
			//Total
			//empty array		
		}

		return $result;
	}

	public function getFieldsByTimeFrame($timeFrame)
	{	
		$result = array();

		switch (strtolower($timeFrame))
		{	
			case "hourly":
			$result = array('y', 'm', 'd', 'h');
			break;
			case "daily" : 
			$result =  array('y', 'm', 'd');
			break;
			case "monthly":
			$result = array('y', 'm');
			break;
			case "yearly":
			$result = array('y');
			break;
			default:
		}

		return $result;
	}

}