<?php 

namespace Yelp\MongoDB\DataInsertion;

use Yelp\MongoDB\DataInsertion\Collection\Composite;
use Exception;

class Index extends Base
{

	function __construct(array $config = array())
	{	
		parent::__construct($config);
	}
	
	public function createIndexes()
	{	

		try{
				
			$composite = $this->getComposite();

			$composite->createIndexes();

			$this->msg("Created indexes succesfully!", "info");

			return $this->returnResponse(true);

		} catch (Exception $e) {
			$this->msg($e->getMessage(), "error");
			return $this->returnResponse(false);
		}

	}

}