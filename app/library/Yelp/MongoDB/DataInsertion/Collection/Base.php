<?php 

namespace Yelp\MongoDB\DataInsertion\Collection;

abstract class Base 
{	

	protected $mongoClient  = null;
	protected $database  = null;


	public function __construct($database, \MongoClient $mongoClient)
	{
		$this->mongoClient = $mongoClient;
		$this->database = $database;
	}


	protected function getMongoClient()
	{
		return $this->mongoClient;
	}


	protected function basicIndex($collName, $indexArr)
	{
		$coll = $this->getMongoClient()->selectCollection($this->database, $collName);

		$mapped = $this->mapColumns($indexArr);

		$this->ensureIndexes($mapped, $coll);
	}

	protected function mapColumns($indexes)
	{	
		$newIndex = array();

		foreach ($indexes as $key => $index) 
		{
			$newIndex[] = array("columns" => $index);
		}

		return $newIndex;

	}


	protected function ensureIndexes($indexes, \MongoCollection $coll)
	{	
		foreach ($indexes as $indexId => $indexData) 
		{	
			$options = array_get($indexData, "options", array());
			$options = $this->getIndexOptions($options);

			$coll->createIndex($indexData["columns"], $options);	
		}
	}

	protected function getIndexOptions($options)
	{
		$defaults = array(
			"socketTimeoutMS" => -1,
			"maxTimeMS" => 0,
			"w" => 0
		);

		return array_merge($defaults, $options);
	}

	abstract public function rowHook(&$row);
	abstract public function createIndexes();

	protected function setMongoDate(&$row, $field)
	{
		$value = array_get($row, $field, null);

		if($value === null) return null;

		$ts = strtotime($value);

		if($ts === false) return false;

		$dateObj = new \DateTime($value, new \DateTimeZone("GMT"));

		$mongoDate = new \MongoDate($dateObj->getTimestamp());

		$row[$field] = $mongoDate;

	}


}