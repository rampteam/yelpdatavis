<?php 

namespace Yelp\MongoDB\DataInsertion\Collection;

class Checkin extends Base 
{
	public function rowHook(&$row)
	{
		
	}


	public function createIndexes()
	{
			
		$indexes = array(
			array("business_id" => 1),
//			array("checkin_info" => 1),
		);

		$this->basicIndex('checkin', $indexes);

	}

}