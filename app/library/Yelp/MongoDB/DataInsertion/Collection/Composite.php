<?php 

namespace Yelp\MongoDB\DataInsertion\Collection;

class Composite extends Base
{
	protected $supported =  array("business", "checkin", "tip", "user", "review");
	protected $collections = array();


	public function __construct($database, \MongoClient $mongoClient)
	{	
		$this->mongoClient = $mongoClient;
		$this->database = $database;
	}

	public function getCollection($key)
	{
		if(!isset($this->collections[$key]))
		{ 
			$this->buildCollection($key);
			
		}

		return $this->collections[$key];
	}

	public function getCollections()
	{
		return $this->collections;
	}

	public function addCollection($key, Base $coll)
	{
		$this->collections[$key] = $coll;
	}

	public function removeCollection($key)
	{
		if(isset($this->collections[$key]))
		{
			unset($this->collections[$key]);
		}
	}

	protected function buildCollection($key)
	{	
		if(array_key_exists($key, $this->collections)) return ;
		if(!in_array($key, $this->supported))
		{
			throw new \InvalidArgumentException("Collection: {$key} not supported!");
		}

		$class =  __NAMESPACE__ . '\\' . ucfirst($key);

		$this->addCollection($key, new $class($this->database, $this->mongoClient));
    
	}

	public function buildCollections(array $collections)
	{
		foreach ($collections as $coll) 
		{
			$this->buildCollection($coll);
		}
	}

	public function loadSupported()
	{
		return $this->buildCollections($this->supported);
	}

	public function rowHook(&$row)
	{	
		foreach ($this->getCollections() as $coll => $obj) 
		{
			 $obj->rowHook($row);
		}
	}

	public function createIndexes()
	{
		foreach ($this->getCollections() as $coll => $obj) 
		{
			 $obj->createIndexes();
		}
	}


}