<?php 

namespace Yelp\MongoDB\DataInsertion\Collection;

class Business extends Base 
{
	public function rowHook(&$row)
	{
		$coord = array(
			"longitude" => null,
			"latitude" => null
		);
		
		foreach ($coord as $key => &$value) {
			
			$value = array_get($row, $key, $value);
			unset($row[$key]);
		}
		extract($coord);
		if(!isset($longitude, $latitude)) return ;
		//correct longitute and latitude coord
		$row["coord"] = array_values($coord); 

		//update state name 
		$this->updateStateName($row);
	}

	protected function updateStateName(&$row)
	{
		$stateAbbr = array(
         "Alabama"       => "AL",
         "Alaska"        => "AK",
         "Arizona"       => "AZ",
         "Arkansas"      => "AR",
         "California"    => "CA",
         "Colorado"      => "CO",
         "Connecticut"   => "CT",
         "Delaware"      => "DE",
         "Florida"       => "FL",
         "Georgia"       => "GA",
         "Hawaii"        => "HI",
         "Idaho"         => "ID",
         "Illinois"      => "IL",
         "Indiana"       => "IN",
         "Iowa"          => "IA",
         "Kansas"        => "KS",
         "Kentucky"      => "KY",
         "Louisiana"     => "LA",
         "Maine"         => "ME",
         "Maryland"      => "MD",
         "Massachusetts" => "MA",
         "Michigan"      => "MI",
         "Minnesota"     => "MN",
         "Mississippi"   => "MS",
         "Missouri"      => "MO",
         "Montana"       => "MT",
         "Nebraska"      => "NE",
         "Nevada"        => "NV",
         "New Hampshire" => "NH",
         "New Jersey"    => "NJ",
         "New Mexico"    => "NM",
         "New York"      => "NY",
         "North Carolina"=> "NC",
         "North Dakota"      => "ND",
         "Ohio"      => "OH",
         "Oklahoma"      => "OK",
         "Oregon"        => "OR",
         "Pennsylvania"      => "PA",
         "Rhode Island"      => "RI",
         "South Carolina"        => "SC",
         "South Dakota"      => "SD",
         "Tennessee"     => "TN",
         "Texas"     => "TX",
         "Utah"      => "UT",
         "Vermont"       => "VT",
         "Virginia"      => "VA",
         "Washington"        => "WA",
         "West Virginia"     => "WV",
         "Wisconsin"     => "WI",
         "Wyoming"       => "WY"
         );

		$abbr = array_flip($stateAbbr);

		if(!isset($row["state"])) return ;

		if(!isset($abbr[$row["state"]])) return;

		$row["state"] = strtolower($abbr[$row["state"]]);

	}

	public function createIndexes()
	{
			
		$indexes = array(
			array("coord" => "2d"),
			array("name" => 1),
			array("business_id" => 1),
			array("city" => 1, "state" => 1),
			array("stars" => 1),
			array("review_count" => 1),
		);

		$this->basicIndex('business', $indexes);

	}

}