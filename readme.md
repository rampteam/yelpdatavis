## Yelp Datavis

## https://www.youtube.com/watch?v=UZjE4xY-t84

## http://creativecommons.org/licenses/by-sa/4.0/

## Installation

## Configure sql database connection in app/config/database.php

## Install mongodb and configure it in app/config/database.php

## Install composer and run composer install in project folder

## To activate caching install redis server and configure it in app/config/cache.php

## Insert the data using 'php artisan app:batch:upsert -f /path/to/data/file'

## To speed up the searches you must create indexes after insert using the command 'php artisan app:batch:create:indexes'

## Javascript and Css files are located in js_dev folder and must be concatenated using grunt.js. Install grunt and run the command: grunt, in project source folder