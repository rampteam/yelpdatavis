module.exports = function(grunt) {

	// Project configuration.
	grunt
		.initConfig({

			pkg: grunt.file.readJSON('package.json'),

			meta: {
				basePath: 'public/static/',
        jsDevPath: 'js_dev/',
				cssDevPath: 'css_dev/',
				jsPath: 'public/static/js/'
			},


			watch: {
				scripts: {
					files: [
            '<%= meta.jsDevPath %>/**/*.js',
						'<%= meta.cssDevPath %>/**/*.css'
					],
					tasks: ['concat', 'uglify:yelpFrondEnd', 'cssmin']
				}
			},

			concat: {
				options: {
					separator: ' ',
				},


				yelp: {
					src: [
            'js_dev/utils/ViewportSize.js',
            'js_dev/utils/capitalize.js',
            'js_dev/utils/isNumeric.js',
            'js_dev/events/event.js',
            'js_dev/template.js',
            'js_dev/models/BaseModel.js',
            'js_dev/models/YBusiness.js',
            'js_dev/models/YBusinessTreemap.js',
            'js_dev/models/YCheckIn.js',
            'js_dev/models/YCity.js',
            'js_dev/models/YReview.js',
            'js_dev/models/YState.js',
            'js_dev/models/YTip.js',
            'js_dev/models/YUser.js',
            'js_dev/models/Filter.js',
            'js_dev/models/PaginationMore.js',
            'js_dev/models/YUserBusiness.js',
            'js_dev/views/BaseView.js',
            'js_dev/views/PaginationMore.js',
            'js_dev/views/index/IndexCircleStatesView.js',
            'js_dev/views/business/BusinessIndexTreemap.js',
            'js_dev/views/business/ShowBusiness.js',
            'js_dev/views/business/BusinessMaps.js',
            'js_dev/views/users/UsersIndexGraph.js',
            'js_dev/views/users/ShowUser.js',
            'js_dev/views/users/UserMaps.js',
            'js_dev/views/reviews/Review.js',
            'js_dev/views/reviews/IndexReview.js',
            'js_dev/views/filters/LimitFilter.js',
            'js_dev/views/filters/OrderByFilter.js',
            'js_dev/views/filters/SearchFilter.js',
            'js_dev/views/filters/NumericFilter.js',
            'js_dev/views/filters/Filters.js',
            'js_dev/views/LoadingView.js',
            'js_dev/collections/YState.js',
            'js_dev/collections/YBusiness.js',
            'js_dev/collections/YBusinessTreemap.js',
            'js_dev/collections/YUsers.js',
            'js_dev/collections/YReviews.js',
            'js_dev/collections/Filters.js',
            'js_dev/collections/YUserBusiness.js',
						'js_dev/routers/router.js'
					],
					dest: 'js_dev/concat/master.js'
				},

				settings: {
					src: [
						'js_dev/settings/init.js'
					],
					dest: 'js_dev/concat/settings.js',
				},

        headLibrary: {
          src: [
            'js_dev/library/modernizr.js',
            'js_dev/library/prefixfree.min.js',
          ],
          dest: 'js_dev/concat/headLibrary.js'
        },

        footerLibrary: {
          src: [
            'js_dev/library/underscore.min.js',
            'js_dev/library/jquery.js',
            'js_dev/library/jquery.ba.bbq.min.js',
            'js_dev/library/backbone.min.js',
            'js_dev/library/foundation.min.js',
            'js_dev/library/d3.min.js',
            'js_dev/library/d3.tooltip.js',
            'js_dev/library/gmaps.js',
          ],
          dest: 'js_dev/concat/footLibrary.js'
        },

        yelpFrondEnd: {
          src: [
            'js_dev/concat/master.js',
            'js_dev/concat/settings.js'
          ],
          dest: 'js_dev/concat/master.js'
        },
			},

			uglify: {
				yelpFrondEnd: {
					files: {
						'public/static/js/master.min.js': 'js_dev/concat/master.js',
					}
				},
        library: {
          files: {
            'public/static/js/footerLibrary.min.js': 'js_dev/concat/footLibrary.js',
            'public/static/js/headLibrary.min.js': 'js_dev/concat/headLibrary.js',
          }
        }
			},

      cssmin: {
        combine: {
          files: {
            'public/static/css/master.min.css': [
              'css_dev/normalize.css',
              'css_dev/foundation.css',
              'css_dev/master.css',
              'css_dev/svg.css',
              'css_dev/spinner.css',
              'css_dev/404.css',
            ]
          }
        }
      }
		});

	// These plugins provide necessary tasks.
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

	// Default task
	grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
};