<?php 

set_time_limit(0);

$file = "../app/storage/data/dataset";
$filew = "../app/storage/data/sdataset";

$data = array();

truncate($file, $filew);
function truncate($file, $filew)
{
		$f = fopen($file, "r");
		$fw = fopen($filew, "w");
        if($f === false || $fw === false)
        {
            throw new \InvalidArgumentException("Cannot open file :!");
        }
	
        $nrLines = 0;
   

        while (!feof($f)) {
            $line = fgets($f);
	
            $row = json_decode($line, true);
            if(json_last_error() !== JSON_ERROR_NONE)  {
               //$this->info("Invalid JSON line encountered!");
                continue;
            }
            //Trailing blank lines
            if(is_array($row))
            {
	
            	$type = empty($row["type"]) ? null : $row["type"];
            	if($type === null)
            	{
            		//$this->info("Unable to determine row type!");
            		continue;
            	}
	
            	if(!isset($data[$type]))
            	{
            		$data[$type] = 0;
            	}

            	$data[$type] ++;
            	if($data[$type] < 1000)
            	{
            		fwrite($fw, $line);
            	}

            }
	
        }
	
        fclose($f);
        fclose($fw);

}