jQuery(document).ready(function($) {
  // loading modal
  Yelp.Loading = new Yelp.Views.LoadingView();
  // 
  Yelp.Router = new Yelp.Routers.Router();
  new Yelp.Views.IndexCircleStatesView({
    collection: new Yelp.Collections.YState()
  });
/*  new Yelp.Views.BusinessIndexCircle({
    collection: new Yelp.Collections.YBusiness()
  });*/
    new Yelp.Views.BusinessIndexTreemap({
    collection: new Yelp.Collections.YBusinessTreemap()
  });
  new Yelp.Views.UserIndexGraph({
    collection: new Yelp.Collections.YUsers()
  });
  new Yelp.Views.Filters({
    collection: new Yelp.Collections.Filters([{
        type: 'limit',
        value: 10,
        name: 'limit',
        label: 'Limit',
        limit: [10, 100, 250, 500, 1000],
      },
      // order by filters 
      {
        type: 'orderBy',
        name: 'stars',
        label: 'Stars',
      }, {
        type: 'orderBy',
        name: 'review_count',
        label: 'Reviews Number',
      }, {
        type: 'orderBy',
        name: 'city',
        label: 'City Name',
      }, {
        type: 'orderBy',
        name: 'state',
        label: 'State Name',
      }, {
        type: 'orderBy',
        name: 'name',
        label: 'Business Name',
      },
      // search filter
      {
        type: 'search',
        name: 'name',
        label: 'Business Name',
      }, {
        type: 'search',
        name: 'city',
        label: 'City Name',
      }, {
        type: 'search',
        name: 'state',
        label: 'State Name',
      }, {
        type: 'search',
        name: 'categories',
        label: 'Category',
      }, {
        type: 'numeric',
        name: 'stars',
        label: 'Stars',
        max: '5',
      }, {
        type: 'numeric',
        name: 'review_count',
        label: 'Reviews',
      },
    ]),
    contentId: 'businessesTab',
    routeName: 'ybusinesses',
    backboneRouteName: 'ybusinesses'
  });
  new Yelp.Views.Filters({
    collection: new Yelp.Collections.Filters([{
        type: 'limit',
        value: 10,
        name: 'limit',
        label: 'Limit'
      },
      // order by filters 
      {
        type: 'orderBy',
        name: 'stars',
        label: 'Stars',
      }, {
        type: 'orderBy',
        name: 'review_count',
        label: 'Reviews Number',
      }, {
        type: 'orderBy',
        name: 'city',
        label: 'City Name',
      }, {
        type: 'orderBy',
        name: 'state',
        label: 'State Name',
      }, {
        type: 'orderBy',
        name: 'name',
        label: 'Business Name',
      },
      // search filter
      {
        type: 'search',
        name: 'name',
        label: 'Business Name',
      }, {
        type: 'search',
        name: 'city',
        label: 'City Name',
      }, {
        type: 'search',
        name: 'state',
        label: 'State Name',
      }, {
        type: 'search',
        name: 'categories',
        label: 'Category',
      }, {
        type: 'numeric',
        name: 'stars',
        label: 'Stars',
        max: '5',
      }, {
        type: 'numeric',
        name: 'review_count',
        label: 'Reviews',
      },
    ]),
    contentId: 'businessesMapsTab',
    routeName: 'ybusinesses/maps',
    backboneRouteName: 'ybusinesses/maps'
  });
  new Yelp.Views.Filters({
    collection: new Yelp.Collections.Filters([{
        type: 'limit',
        value: 100,
        name: 'limit',
        label: 'Limit'
      },
      // order by filters 
      {
        type: 'orderBy',
        name: 'average_stars',
        label: 'Average Stars',
      }, {
        type: 'orderBy',
        name: 'review_count',
        label: 'Reviews Number',
      }, {
        type: 'orderBy',
        name: 'fans',
        label: 'Number of Fans',
      },
      // search filter
      {
        type: 'search',
        name: 'name',
        label: 'Name',
      }, {
        type: 'search',
        name: 'city',
        label: 'City Name',
      }, {
        type: 'search',
        name: 'state',
        label: 'State Name',
      }, {
        type: 'numeric',
        name: 'average_stars',
        label: 'Average Stars',
        max: '5',
      }, {
        type: 'numeric',
        name: 'review_count',
        label: 'Number of Reviews',
      }, {
        type: 'numeric',
        name: 'fans',
        label: 'Number of Fans',
      }
    ]),
    contentId: 'usersTab',
    routeName: 'yusers',
    backboneRouteName: 'yusers',
  });
  new Yelp.Views.Filters({
    collection: new Yelp.Collections.Filters([
      // order by filters 
      {
        type: 'orderBy',
        name: 'stars',
        label: 'Stars',
      },
      // search filter
      {
        type: 'search',
        name: 'text',
        label: 'Text',
      }, {
        type: 'numeric',
        name: 'stars',
        label: 'Stars',
        max: '5',
      },{
        type: 'search',
        name: 'city',
        label: 'City Name',
      }, {
        type: 'search',
        name: 'state',
        label: 'State Name',
      },
      {
        type: 'search',
        name: 'businessname',
        label: 'Business Name',
      },
      {
        type: 'search',
        name: 'username',
        label: 'User Name',
      },
    ]),
    contentId: 'reviewsTab',
    routeName: 'yreviews',
    backboneRouteName: 'yreviews',
  });
  new Yelp.Views.ShowBusiness({
    model: new Yelp.Models.YBusiness()
  });
  new Yelp.Views.ShowUser({
    model: new Yelp.Models.YUser()
  });
  new Yelp.Views.BusinessMaps({
    collection: new Yelp.Collections.YBusiness()
  });
  new Yelp.Views.IndexReview({
    collection: new Yelp.Collections.YReviews()
  });
  Backbone.history.start();
});