var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

  "use strict";

  /**
   * Business Treemap Model
   */

  Yelp.Models.YBusinessTreemap = Yelp.Models.BaseModel.extend({

    defaults: {
      name: '',
      children: [], 
      size: 0
    },

    getSvgInfo: function() {

      var data = {}
      data["name"] = this.get('name');
      var size = this.get('size');

      if (size != 0) {
        data["value"] = size
      }
      
      if (this.get('children').length) {
        data["children"] = this.get('children')
      }
      
      return data;

    },

    urlRoot: 'api/ybusinesses/treemap',
  });

}();