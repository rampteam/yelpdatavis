var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

  "use strict";

  /**
   * Review Model
   */

  Yelp.Models.YCity = Yelp.Models.BaseModel.extend({
    defaults: {
      long_name: '',
      cities: [],
    },
    initialize: function() {
      // bind this to methods
      _.bindAll(this, 'getSvgInfo');
    },

    getSvgInfo: function() {
      var states = {
        'name': this.get('long_name'),
        'cities': [],
      };
      _.each(this.get('cities'), function(value, key, list) {
        states['cities'].push(value['name']);
      });
      return states;
    },
  });
}();