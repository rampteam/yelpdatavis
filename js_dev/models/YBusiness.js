var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

  "use strict";

  /**
   * Business Model
   */

  Yelp.Models.YBusiness = Yelp.Models.BaseModel.extend({

    idAttribute: 'business_id',

    defaults: {
      business_id: '',
      name: '',
      neighborhoods: [],
      full_address: '',
      city: '',
      state: '',
      coord: [],
      stars: 0,
      review_count: 0,
      categories: [],
      open: '',
      hours: {},
      attributes: {},
      filter: ['stars', 'review_count']
    },

    getSvgInfo: function() {
      var size = 1;
      var filters = this.get('filter');

      for (var i = filters.length - 1; i >= 0; i--) {
        size *= this.get(filters[i]);
      }
      return {
        name: this.get('name'),
        business_id: this.get('business_id'),
        children: [{
          name: this.get('name'),
          size: size
        }],
      };
    },

    urlRoot: 'api/ybusinesses',
  });

}();