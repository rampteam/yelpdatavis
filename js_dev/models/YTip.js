var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

	"use strict";

	/**
	 * Tip Model
	 */

	Yelp.Models.YTip = Yelp.Models.BaseModel.extend({

		defaults: {
			text: '',
			business_id: '',
			user_id: '',
			date: '',
			likes: 0,
		}
	});
}();
