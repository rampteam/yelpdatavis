var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

  "use strict";

  /**
   * Check in Model
   */

  Yelp.Models.Filter = Yelp.Models.BaseModel.extend({

    defaults: {
      type: '', // orderBy, filterSearch, filterNumeric, limit 
      active: false,
      name: '',
      label: '',
      value: '',
      limit: [10, 100, 250],
      orderType: ['ASC', 'DESC'],
      max: '',
      numericRange: {
        'eq': '',
        'lower': {
          'lte': '',
          'lt': '',
        },
        'upper': {
          'gte': '',
          'gt': '',
        },
      },
    },
    validTypes: ['orderBy', 'search', 'numeric', 'limit'],

    reset: function() {
      this.resetNumeric();
      if (this.get('type') == 'limit') {
        this.set('value', 10);
      } else {
        this.set('value', '');
      }
    },


    getNumericValues: function() {
      var ranges = this.get('numericRange');
      return {
        'eq': ranges.eq,
        'lte': ranges.lower.lte,
        'lt': ranges.lower.lt,
        'gt': ranges.upper.gt,
        'gte': ranges.upper.gte,
      };
    },

    getNumeric: function(blank) {
      if (typeof(blank) !== 'undefined' && blank === true)
        return {
          'eq': '',
          'lower': {
            'lte': '',
            'lt': '',
          },
          'upper': {
            'gte': '',
            'gt': '',
          }
        };
      return this.get('numericRange');
    },

    resetNumeric: function() {
      this.set('numericRange', this.getNumeric(true));
    },

    setNumeric: function(type, value) {
      value = parseInt(value, 10);
      var numeric = this.getNumeric();
      if (type == 'eq') {
        numeric = this.getNumeric(true);
        numeric.eq = value;
      } else if (type == 'gte' || type == 'gt') {
        numeric.eq = numeric.upper.gte = numeric.upper.gt = '';
        numeric.upper[type] = value;
      } else if (type == 'lte' || type == 'lt') {
        numeric.eq = numeric.lower.lte = numeric.lower.lt = '';
        numeric.lower[type] = value;
      }
      this.set('numericRange', numeric);
    },

    validate: function(attrs) {
      var errors = [];
      if (attrs.type && this.validTypes.indexOf(attrs.type) === -1) {
        errors.push({
          type: 'Invalid filter type'
        });
      } else {
        this[attrs.type + 'Validate'](attrs, errors);
      }
      return errors.length > 0 ? errors : false;
    },

    getValue: function() {
      if (this.get('type') == 'limit')
        return this.get('value', 10);

      if (this.get('type') == 'numeric') {
        var queryObj = {};
        var values = this.getNumericValues();
        for (var name in values) {
          if (values[name] !== '') {
            queryObj[name] = values[name];
          }
        }
        if (Object.keys(queryObj).length === 0) {
          return '';
        }
        return queryObj;
      }

      return this.get('value');
    },

    // validate order by filter
    orderByValidate: function(attrs, errors) {
      if (attrs.value && this.get('orderType').indexOf(attrs.value) === -1) {
        errors.push({
          order: 'Invalid order for ' + this.get('label') + ':' + attrs.value
        });
      }
    },

    // validate search filter
    searchValidate: function(attrs, errors) {},

    // validate numeric filter
    numericValidate: function(attrs, errors) {
      var range = attrs.numericRange;
      if (range.lower.lt < 0 || range.lower.lte < 0 || range.eq < 0 || range.upper.gt < 0 || range.upper.gte < 0) {
        errors.push({
          orderBy: 'Filters can not be nagetive in ' + attrs.name,
        });
        return;
      }
      if (range.lower.lt) {
        if (
          (range.upper.gt && range.lower.lt <= range.upper.gt) ||
          (range.upper.gte && range.lower.lt <= range.upper.gte)
        ) {
          errors.push({
            orderBy: 'Lower value can not be grater or equal with greater value in ' + attrs.name,
          });
          return;
        }
      } else if (range.lower.lte) {
        if (
          (range.upper.gt && range.lower.lte <= range.upper.gt) ||
          (range.upper.gte && range.lower.lte < range.upper.gte)
        ) {
          errors.push({
            orderBy: 'Lower value can not be grater or equal with greater value in ' + attrs.name,
          });
          return;
        }
      }
    },

    // validate limit filter
    limitValidate: function(attrs, errors) {
      if (!attrs.value || this.get('limit').indexOf(parseInt(attrs.value, 10)) === -1) {
        errors.push({
          limit: 'Invalid limit ' + attrs.value
        });
      }
    },
  });
}();