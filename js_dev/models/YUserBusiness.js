var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

  "use strict";

  /**
   * Business Model
   */

   Yelp.Models.YUserBusiness = Yelp.Models.BaseModel.extend({

    idAttribute: 'business_id',

    defaults: {
      "nr_reviews": 0,
      "avg_stars": 0,
      "user_id": "",
      "business_id": "",
      "nr_tips": 0,
      "avg_likes": 0
    },

    urlRoot: 'api/yusers/businesses',
  });

 }();