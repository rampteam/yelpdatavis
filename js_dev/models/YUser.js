var Yelp = Yelp || {};
Yelp.Models = Yelp.Models || {};

! function() {

  "use strict";

  /**
   * Partner Model
   */

  Yelp.Models.YUser = Yelp.Models.BaseModel.extend({
    urlRoot: 'api/yusers',
    idAttribute: 'user_id',
    defaults: {
      user_id: '',
      name: '',
      review_count: 0,
      average_stars: 0.0,
      votes: {},
      friends: [],
      elite: [],
      yelping_since: '',
      compliments: {},
      fans: 0,
      index: null,
      links: [],
    },

    getGraphJson: function() {
      return {
        index: this.get('index'),
        links: this.get('links'),
        score: Math.max(4, parseInt(this.get('average_stars'), 10)) * 2,
        id: this.get('user_id'),
        name: this.get('name'),
      };
    },

    getTotalCompliments: function() {
      var total = 0;
      var compliments = this.get('compliments');
      for (var name in compliments) {
        total += parseInt(compliments[name], 10);
      }
      return total;
    },

    getChartCompliments: function() {
      var data = [];
      var compliments = this.get('compliments');
      for( var label in compliments) {
        data.push({label: label, value: compliments[label]});
      }
      return data;
    },
  });
}();