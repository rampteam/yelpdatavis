var Yelp = Yelp || {};
Yelp.Collections = Yelp.Collections || {};

!(function() {

  "use strict";
  
  Yelp.Collections.YUserBusiness = Backbone.Collection.extend({
    model: Yelp.Models.YUserBusiness, 
    url: 'api/yusers/businesses',
    
  });
})();