var Yelp = Yelp || {};
Yelp.Collections = Yelp.Collections || {};

!(function() {

  "use strict";

  Yelp.Collections.YUsers = Backbone.Collection.extend({
    model: Yelp.Models.YUser,
    url: 'api/yusers/fgraph',
    edges: [],

    modelIndex: 0,

    getFriendsGraphJson: function() {
      var graphJson = {
        'nodes': [],
        'edges': [],
      };
      var uid = this.userId();
      var self = this;
      this.each(function(el, index) {
        if (el.get('user_id') == uid) {
          self.modelIndex = graphJson.nodes.length;
        }
        graphJson.nodes.push(el.getGraphJson());
      });
      graphJson.edges = this.getEdges().slice(0, 500);
      return graphJson;
    },

    userId: function() {
      return Yelp.Router.getCurrentRoute().split('/').pop();
    },

    getEdges: function() {
      var self = this;
      if (this.edges.length < 500) {
        return this.edges;
      }
      var edges = this.edges.filter(function(el) {
        return el.source != self.modelIndex && el.target != self.modelIndex;
      });
      return edges;

    },

    getGraphJson: function() {
      var graphJson = {
        'nodes': [],
        'edges': this.edges.slice(0, 750),
      };
      this.each(function(el, index) {
        graphJson.nodes.push(el.getGraphJson());
      });
      return graphJson;
    },

    set: function(models, options) {
      if (models !== undefined && models.hasOwnProperty('users')) {
        this.reset();
        this.edges = models.links;
        _.each(models.users, function(value, key, list) {
          this.models.push(new Yelp.Models.YUser(value));
        }, this);
      } else {
        this.edges = [];
        Yelp.Collections.YUsers.__super__.set.call(this);
      }
    }
  });
})();