var Yelp = Yelp || {};
Yelp.Collections = Yelp.Collections || {};

!(function() {

  "use strict";

  Yelp.Collections.YBusinessTreemap = Backbone.Collection.extend({
    model: Yelp.Models.YBusinessTreemap,
    url: 'api/ybusinesses/treemap',

    getSvgJson: function() {
      var svgJson = {
        name: 'Businesses',
        children: []
      };
      this.each(function(el, index) {
        svgJson.children.push(el.getSvgInfo());
      });
      if (Object.keys(svgJson.children).length === 0) {
        return {};
      }
      return svgJson;
    },
  });
})();