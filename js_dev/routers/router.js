var Yelp = Yelp || {};
Yelp.Routers = Yelp.Routers || {};

! function() {

  "use strict";

  Yelp.Routers.Router = Backbone.Router.extend({
    routes: {
      '': 'index',
      'index/country/:country': 'index',
      'index/country/:country/state/:state': 'index',
      'ybusinesses?*query': 'ybusinessesIndexQuery',
      'ybusinesses': 'ybusinessesIndex',
      'ybusinesses/maps?*query': 'ybusinessesMapsQuery',
      'ybusinesses/maps': 'ybusinessesMapsIndex',
      'ybusinesses/:id': 'ybusinessesShow',
      'yusers?*query': 'yusersIndex',
      'yusers': 'yusersIndex',
      'yusers/:id': 'yusersShow',
      'yusers/friends-graph/:id': 'yusersFriendsGhaph',
      'yreviews/*query': 'yreviewsIndex',
      'yreviews': 'yreviewsIndex',
      '*path':  'notFoundRoute',
    },

    buildRoute: function(routeName, params) {
      if (typeof(params) !== 'undefined') {
        routeName += '?' + $.param(params);
      }
      return routeName;
    },

    setRoute: function(routeName, params) {
      this.navigate(this.buildRoute(routeName, params), {
        trigger: true,
      });
    },

    getCurrentRoute: function() {
      return Backbone.history.fragment.split('?')[0];
    },

    getCurrentQueryObject: function() {
      var route = Backbone.history.fragment.split('?');
      if (route.length <= 1) {
        return null;
      }
      return $.deparam.querystring(route[1]);
    },

    all: function() {
      Yelp.Loading.open();
      Yelp.Events.trigger('changed');
      $('.reveal-modal').foundation('reveal').foundation('reveal', 'close');
    },

    index: function(country, state) {
      this.all();
      Yelp.Events.trigger('index:circle', country, state);
    },

    ybusinessesIndex: function() {
      var query = $.param({
        'orderBy': {
          'stars': 'DESC',
          'review_count': 'DESC'
        },
        'limit': 1000,
      });
      this.navigate('ybusinesses?' + query, {
        trigger: true
      });
    },

    ybusinessesIndexQuery: function(query) {
      this.all();
      Yelp.Events.trigger('ybussinesses:index', query);
    },

    ybusinessesShow: function(id) {
      this.all();
      Yelp.Events.trigger('ybussinesses:show', id);
    },

    ybusinessesMapsIndex: function() {
      var query = $.param({
        'limit': 100
      });
      this.navigate('ybusinesses/maps?' + query, {
        trigger: true
      });
    },
    ybusinessesMapsQuery: function(query) {
      this.all();
      Yelp.Events.trigger('ybussinesses:maps', query);
    },


    yusersIndex: function(query) {
      this.all();
      Yelp.Events.trigger('yusers:index', query);
    },

    yusersShow: function(id) {
      this.all();
      Yelp.Events.trigger('yusers:show', id);
    },

    yusersFriendsGhaph: function(id) {
      this.all();
      Yelp.Events.trigger('yusers:friends-graph', id);
    },

    yreviewsIndex: function(query) {
      this.all();
      Yelp.Events.trigger('yreviews:index', query);
    },

    notFoundRoute: function() {
      var template404 = Yelp.template('404Template');
      $('#content').empty().html(template404());
      Yelp.Loading.close();
    },
  });
}();