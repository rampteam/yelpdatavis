var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.Review = Yelp.Views.BaseView.extend({
    model: Yelp.Models.YReview,
    tagName: 'div',
    className: 'row yreview',
    // load template from master.blade.php
    template: Yelp.template('reviewTemplate'),
    // constructor method
    initialize: function(models, options) {
      // call parent constructor
      Yelp.Views.Review.__super__.initialize.call(this);
    },

    render: function() {
      this.$el.html(this.getTemplate());
      // used for method chaining
      return this;
    },

    getTemplate: function() {
      return  this.template({
        businessName: this.model.get('business').name,
        businessId: this.model.get('business').business_id,
        date: this.model.get('date'),
        stars: parseInt(this.model.get('stars'),10),
        userId: this.model.get('user').user_id,
        userName: this.model.get('user').name,
        text: this.model.get('text'),
        votes: this.model.get('votes'),
      });
    },
  });
}();