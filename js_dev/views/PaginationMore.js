var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.PaginationMore = Yelp.Views.BaseView.extend({
    model: Yelp.Models.PaginationMore,
    tagName: 'div',
    className: 'large-12 columns more-button',
    // constructor method
    initialize: function(models, options) {
      // call parent constructor
      Yelp.Views.PaginationMore.__super__.initialize.call(this);
    },

    render: function() {
      var self = this;
      // set content
      this.$el.html('More');
      // bind event on click
      this.$el.click(function(event) {
        // stop propagation of event to parent elements
        event.stopPropagation();
        // trigger a custom events
        Yelp.Events.trigger(self.model.get('eventName'));
      });
      // used for method chaining
      return this;
    },
  });
}();
