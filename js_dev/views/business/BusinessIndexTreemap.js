var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.BusinessIndexTreemap = Yelp.Views.BaseView.extend({
    tagName: 'div',
    margin: 20,
    className: 'business-treemap',
    // svg 
    d3: null,
    color: null,
    pack: null,
    // 
    routing: false,

    initialize: function() {
      Yelp.Views.BusinessIndexTreemap.__super__.initialize.call(this);
      Yelp.Events.on('ybussinesses:index', this.show, this);
      _.bindAll(this, 'render', 'buildSvg');
      this.setSvgInfo();
    },

    setSvgInfo: function() {
      
    },


    show: function(query) {
      this.changed = false;
      this.$content.empty();
      this.collection.fetch({
        data: query,
        success: this.render
      });
    },

    render: function() {
      this.$content.empty();
      this.$content.append(this.$el.empty());
      this.buildSvg();
      if (this.changed) {
        this.$el.empty();
      } else {
        this.$el.show();
        Yelp.Loading.close();
      }
      return this;
    },

    buildSvg: function() {
      // build svg json

      var self = this;
      var svgJson = this.collection.getSvgJson();
     
      //console.log(JSON.stringify(svgJson));

      if (Object.keys(svgJson).length === 0) {
        self.$el.append('<div class="text-center"><h2> No businesses found </h2></div>');
        return;
      }

      var margin = {top: 30, right: 0, bottom: 0, left: 0},
        width = self.diameter,
        height = 560 - margin.top - margin.bottom,
        formatNumber = d3.format(",d"), transitioning,
        color = d3.scale.category20c();

      var x = d3.scale.linear()
        .domain([0, width]) 
        .range([0, width]);

      var y = d3.scale.linear()
        .domain([0, height])
        .range([0, height]);


      function fontSize(d, i) {
          var size = d.dx / 5;
          var words = d.name.split(' ');
          var word = words[0];
          var width = d.dx;
          var height = d.dy;
          var length = 0;
          d3.select(this).style("font-size", size + "px").text(word);
          while (((this.getBBox().width >= width) || (this.getBBox().height >= height)) && (size > 12)) {
              size--;
              d3.select(this).style("font-size", size + "px");
              this.firstChild.data = word;
          }
      }

      function wordWrap(d, i) {
          var words = d.name.split(' ');
          var line = new Array();
          var length = 0;
          var text = "";
          var width = d.dx;
          var height = d.dy;
          var word;
          do {
              word = words.shift();
              line.push(word);
              if (words.length)
                  this.firstChild.data = line.join(' ') + " " + words[0];
              else
                  this.firstChild.data = line.join(' ');
              length = this.getBBox().width;
              if (length < width && words.length) {;
              } else {
                  text = line.join(' ');
                  this.firstChild.data = text;
                  if (this.getBBox().width > width) {
                      text = d3.select(this).select(function() {
                          return this.lastChild;
                      }).text();
                      text = text + "...";
                      d3.select(this).select(function() {
                          return this.lastChild;
                      }).text(text);
                      d3.select(this).classed("wordwrapped", true);
                      break;
                  } else
                  ;

                  if (text != '') {
                      d3.select(this).append("svg:tspan")
                          .attr("x", 0)
                          .attr("dx", "0.15em")
                          .attr("dy", "0.9em")
                          .text(text);
                  } else
                  ;

                  if (this.getBBox().height > height && words.length) {
                      text = d3.select(this).select(function() {
                          return this.lastChild;
                      }).text();
                      text = text + "...";
                      d3.select(this).select(function() {
                          return this.lastChild;
                      }).text(text);
                      d3.select(this).classed("wordwrapped", true);

                      break;
                  } else
                  ;

                  line = new Array();
              }
          } while (words.length);
          this.firstChild.data = '';
      }

      var treemap = d3.layout.treemap()
        .children(function(d, depth) { return depth ? null : d._children; })
        .sort(function(a, b) { return a.value - b.value; })
        .ratio(height / width * 0.5 * (1 + Math.sqrt(5)))
        .round(false);

      var svg = d3.select(this.el).append("svg")
        /*.attr('width', self.diameter)
        .attr('height', self.diameter)
        .append("g");*/
        .attr("width", width + margin.left + margin.right) 
        .attr("height", height + margin.bottom + margin.top)
        .style("margin-left", -margin.left + "px")
        .style("margin.right", -margin.right + "px")
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .style("shape-rendering", "crispEdges");

      var grandparent = svg.append("g")
      .attr("class", "grandparent");

      grandparent.append("rect")
          .attr("y", -margin.top)
          .attr("width", width)
          .attr("height", margin.top);
      
      grandparent.append("text")
          .attr("x", 6)
          .attr("y", 6 - margin.top)
          .attr("dy", ".75em");

      initialize(svgJson);
      accumulate(svgJson);
      layout(svgJson);
      display(svgJson);
    
      function initialize(root) {
        root.x = root.y = 0;
        root.dx = width;
        root.dy = height;
        root.depth = 0;
      }
    
      // Aggregate the values for internal nodes. This is normally done by the
      // treemap layout, but not here because of our custom implementation.
      // We also take a snapshot of the original children (_children) to avoid
      // the children being overwritten when when layout is computed.
      function accumulate(d) {
        return (d._children = d.children)
            ? d.value = d.children.reduce(function(p, v) { return p + accumulate(v); }, 0)
            : d.value;
      }
    
      // Compute the treemap layout recursively such that each group of siblings
      // uses the same size (1×1) rather than the dimensions of the parent cell.
      // This optimizes the layout for the current zoom state. Note that a wrapper
      // object is created for the parent node for each group of siblings so that
      // the parent’s dimensions are not discarded as we recurse. Since each group
      // of sibling was laid out in 1×1, we must rescale to fit using absolute
      // coordinates. This lets us use a viewport to zoom.
      function layout(d) {
        if (d._children) {
          treemap.nodes({_children: d._children});
          d._children.forEach(function(c) {
            c.x = d.x + c.x * d.dx;
            c.y = d.y + c.y * d.dy;
            c.dx *= d.dx;
            c.dy *= d.dy;
            c.parent = d;
            layout(c);
          });
        }
      }

      function display(d) {
        grandparent
            .datum(d.parent)
            .on("click", transition)
          .select("text")
            .text(name(d));
    
        var g1 = svg.insert("g", ".grandparent")
            .datum(d)
            .attr("class", "depth");
    
        var g = g1.selectAll("g")
            .data(d._children)
          .enter().append("g");
    
        g.filter(function(d) { return d._children; })
            .classed("children", true)
            .on("click", transition);
    
        g.selectAll(".child")
            .data(function(d) { return d._children || [d]; })
          .enter().append("rect")
            .attr("class", "child") 
            .on("click", function(d){ 
              Yelp.Router.setRoute('ybusinesses/maps', {
              filter: {
                city: d.name,
              },
          });
            })
            .style("fill", function(d) {
                return color(d.parent.name); 
              })
            .call(rect);
    
        g.append("rect")
            .attr("class", "parent")
            .call(rect)
          .append("title")
            .text(function(d) { return d.name; });
    
        g.append("text")
            .text(function(d) { return d.name; })
            .call(text)
            .call(dynamicLabel);
    
        function transition(d) {
          if (transitioning || !d) return;
          transitioning = true;
    
          var g2 = display(d),
              t1 = g1.transition().duration(750),
              t2 = g2.transition().duration(750);
    
          // Update the domain only after entering new elements.
          x.domain([d.x, d.x + d.dx]);
          y.domain([d.y, d.y + d.dy]);
    
          // Enable anti-aliasing during the transition.
          svg.style("shape-rendering", null);
    
          // Draw child nodes on top of parent nodes.
          svg.selectAll(".depth").sort(function(a, b) { return a.depth - b.depth; });
    
          // Fade-in entering text.
          g2.selectAll("text").style("fill-opacity", 0);
    
          // Transition to the new view.
          t1.selectAll("text").call(text).style("fill-opacity", 0);
          t2.selectAll("text").call(text).style("fill-opacity", 1);
          t1.selectAll("rect").call(rect);
          t2.selectAll("rect").call(rect);
    
          // Remove the old node when the transition is finished.
          t1.remove().each("end", function() {
            svg.style("shape-rendering", "crispEdges");
            transitioning = false;
          });
        }

        return g;
    }

      function text(text) {
          text.attr("x", function(d) { return x(d.x) + 6; })
          .attr("y", function(d) { return y(d.y) + 6; })
      }

      function dynamicLabel(text){
        text
        .each(fontSize)
        .attr("dy", ".9em");
      }
  
      function rect(rect) {
        rect.attr("x", function(d) { return x(d.x); })
            .attr("y", function(d) { return y(d.y); })
            .attr("width", function(d) { return x(d.x + d.dx) - x(d.x); })
            .attr("height", function(d) { return y(d.y + d.dy) - y(d.y); });
      }
  
      function name(d) {
        return d.parent
            ? name(d.parent) + "." + d.name
            : d.name;
      }

      return this;
    },

    setNewRoute: function(node) {
      if (this.routing === false) {
        this.routing = true;
        if (node.business_id) {
          Yelp.Router.setRoute('ybusinesses/' + node.business_id);
        }
        this.routing = false;
      }
    },
  });
}();