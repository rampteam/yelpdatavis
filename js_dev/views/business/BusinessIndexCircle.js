var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.BusinessIndexCircle = Yelp.Views.BaseView.extend({
    tagName: 'div',
    margin: 20,
    // svg 
    d3: null,
    color: null,
    pack: null,
    // 
    routing: false,

    initialize: function() {
      Yelp.Views.BusinessIndexCircle.__super__.initialize.call(this);
      Yelp.Events.on('ybussinesses:index', this.show, this);
      _.bindAll(this, 'render', 'buildSvg');
      this.setSvgInfo();
    },

    setSvgInfo: function() {
      this.color = d3.scale.linear()
        .domain([-1, 5])
        .range(['hsl(152,80%,80%)', 'hsl(228,30%,40%)'])
        .interpolate(d3.interpolateHcl);

      // containement to represent the hierarchy, each element size will be determined by children size (leaf size eventualy)
      this.pack = d3.layout.pack()
        .padding(2)
        .size([this.diameter - this.margin, this.diameter - this.margin])
        .value(function(d) {
          return d.size;
        });
    },


    show: function(query) {
      this.changed = false;
      this.$content.empty();
      this.collection.fetch({
        data: query,
        success: this.render
      });
    },

    render: function() {
      this.$content.empty();
      this.$content.append(this.$el.empty());
      this.buildSvg();
      if (this.changed) {
        this.$el.empty();
      } else {
        this.$el.show();
        Yelp.Loading.close();
      }
      return this;
    },

    buildSvg: function() {
      // build svg json

      var self = this;
      var svgJson = this.collection.getSvgJson();
 
      if (Object.keys(svgJson).length === 0) {
        self.$el.append('<div class="text-center"><h2> No businesses found </h2></div>');
        return;
      }

      var nodes = this.pack.nodes(svgJson);
      var focus = svgJson;
      var view;

      // create svg and append a container
      this.svg = d3.select(this.el).append('svg')
        .attr('width', self.diameter)
        .attr('height', self.diameter)
        .call(d3.behavior.zoom().scaleExtent([1, 200]).on("zoom", zoom))
        .append("g");

      function zoom() {
        self.svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
      }

      var circle = this.svg.selectAll("circle")
        .data(nodes)
        .enter()
        .append('circle')
        .attr('class', function(node) {
          if (node.parent) {
            if (node.children)
              return 'node';
            return 'node node--leaf';
          }
          return 'node node--root';
        })
        .style('fill', function(node) {
          if (node.children)
            return self.color(node.depth);
          return null;
        })
        .on('click', function(node) {
          d3.event.stopPropagation();
          self.setNewRoute(node);
        });


      var text = this.svg.selectAll('text')
        .data(nodes)
        .enter()
        .append('text')
        .attr('class', 'businesses-label')
        .style('fill-opacity', function(node) {
          return node.parent === svgJson ? 1 : 0;
        })
        .text(function(node) {
          if (node.name && node.r * 8 < node.name.length) {
            return node.name.substring(0, node.r * 8 - 3) + '...';
          }
          return node.name;
        })
        .style('font-size', function(node) {
          var size = (2 * node.r - 8) / this.getComputedTextLength() * 24;
          if (size < 1)
            return '0.5px';
          return size + 'px';
        })
        .attr('dy', '.35em');


      var node = this.svg.selectAll("circle,text");


      zoomTo([0, 0, svgJson.r * 2 + self.margin]);

      function zoomTo(v) {
        var k = self.diameter / v[2];
        view = v;
        node.attr("transform", function(d) {
          return "translate(" + (d.x - v[0]) * k + "," + (d.y - v[1]) * k + ")";
        });
        circle.attr("r", function(d) {
          return d.r * k;
        });
      }

      return this;
    },

    setNewRoute: function(node) {
      if (this.routing === false) {
        this.routing = true;
        if (node.business_id) {
          Yelp.Router.setRoute('ybusinesses/' + node.business_id);
        }
        this.routing = false;
      }
    },
  });
}();