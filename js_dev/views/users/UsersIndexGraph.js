var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.UserIndexGraph = Yelp.Views.BaseView.extend({
    modal: Yelp.template('userGraphModal'),

    tagName: 'div',
    margin: 20,
    // svg 
    d3: null,
    // 
    routing: false,
    //
    selectedUser: null,
    showThreshhold: 250,

    edgeWeight: 2,

    nodeSize: undefined,
    edgeWidth: undefined,

    xyScale: undefined,
    zoomScale: undefined,

    // vars to keep graph state
    nodes: undefined,
    edges: undefined,
    labels: undefined,
    graphData: undefined,
    activeNode: undefined,
    svg: undefined,
    currentOffset: {
      x: 0,
      y: 0,
    },
    currentZoom: 1.0,

    initialize: function() {
      Yelp.Views.UserIndexGraph.__super__.initialize.call(this);
      Yelp.Events.on('yusers:index', this.show, this);
      Yelp.Events.on('yusers:friends-graph', this.friendsGraph, this);

      this.initializeContstant();
      _.bindAll(this, 'renderFgraph', 'render', 'buildGraph', 'graphDrag', 'graphZoom', 'graphReposition', 'resetVars', 'nodeHighlight');

    },

    initializeContstant: function() {
      this.xyScale = d3.scale.linear().domain([0, this.diameter]).range([0, this.diameter]);
      this.zoomScale = d3.scale.linear().domain([1, 6]).range([1, 6]).clamp(true);
      // nodes and edge sizes
      this.nodeSize = d3.scale.linear().domain([5, 10]).range([1, 16]).clamp(true);
      this.edgeWidth = d3.scale.pow().exponent(8).domain([self.edgeWeight - 2, self.edgeWeight + 2]).range([1, 5]).clamp(true);
    },

    friendsGraph: function(id) {
      this.changed = false;
      this.$content.empty();
      this.collection.url = 'api/yusers/fgraph/' + id;
      this.collection.fetch({
        success: this.renderFgraph
      });
    },

    show: function(query) {
      this.changed = false;
      this.$content.empty();
      this.collection.url = 'api/yusers/fgraph';
      this.collection.fetch({
        data: query,
        success: this.render
      });
    },

    renderFgraph: function() {
      var data = this.collection.getFriendsGraphJson();
      this.$content.append(this.$el.empty());
      this.buildGraph(data);
      if (this.changed) {
        this.$el.empty();
      } else {
        this.$el.show();
        Yelp.Loading.close();
      }
      return this;
    },

    render: function() {
      this.$content.append(this.$el.empty());
      this.buildGraph();
      if (this.changed) {
        this.$el.empty();
      } else {
        this.$el.show();
        Yelp.Loading.close();
      }
      return this;
    },

    resetVars: function() {
      this.activeNode = this.graphData = this.nodes = this.edges = this.labels = undefined;
      this.svg = undefined;
      this.currentOffset = {
        x: 0,
        y: 0,
      };
      this.currentZoom = 1.0;
    },

    buildGraph: function(data) {
      this.resetVars();
      var graph;
      var self = this;
      if (data === undefined) {
        self.graphData = self.collection.getGraphJson();
      } else {
        self.graphData = data;
      }
      // initialize force directed graph
      var fGraph = d3.layout.force()
        .charge(-320)
        .size([self.diameter - self.margin, self.diameter - self.margin])
        .linkStrength(0.1)
        .gravity(0.2)
        .nodes(self.graphData.nodes)
        .links(self.graphData.edges)
        .start();

      // initialize svg
      self.svg = d3.select(self.el).append('svg')
        .attr('width', self.diameter)
        .attr('height', self.diameter)
        .attr("viewBox", "0 0 " + self.diameter + " " + self.diameter)
        .attr("preserveAspectRatio", "xMidYMid meet")
        .attr('class', 'users-graph');

      // drag & zoom
      self.svg.call(
        d3.behavior.drag().on('drag', self.graphDrag)
      );

      self.svg.call(
        d3.behavior.zoom()
        .x(self.xyScale)
        .y(self.xyScale)
        .scaleExtent([1, 6])
        .on('zoom', self.graphZoom)
      );

      graph = self.svg.append('g').attr('class', 'graph-parent');

      // edges 
      self.edges = graph.append('g')
        .attr('class', 'graph graph-edges')
        .selectAll('line')
        .data(self.graphData.edges, function(edge) {
          return edge.source.id + '-' + edge.target.id;
        })
        .enter()
        .append('line')
        .style('stroke-width', function(edge) {
          return self.edgeWidth(self.edgeWeight);
        })
        .attr('class', 'edge');

      // nodes
      self.nodes = graph.append('g')
        .attr('class', 'graph graph-nodes')
        .selectAll('circle')
        .data(self.graphData.nodes, function(node) {
          return node.name;
        })
        .enter()
        .append('circle')
        .attr('id', function(node) {
          return "node-" + node.index;
        })
        .attr('class', function(node) {
          return 'level-' + Math.floor((Math.random() * 3) + 1);
        })
        .attr('r', function(node) {
          return self.nodeSize(node.score);
        })
        .attr('pointer-events', 'all')
        .on('click', function(node) {
          if (node.activate === true) {
            self.nodeModal(node);
          } else {
            self.nodeHighlight(node, false);
            self.nodeHighlight(node, true, true);
          }
        })
        .on('mouseover', function(node) {
          self.nodeHighlight(node, true);
        })
        .on('mouseleave', function(node) {
          self.nodeHighlight(node, false);
        });

      // labels 
      self.labels = graph.append('g')
        .attr('class', 'graph graph-label')
        .selectAll('g.label')
        .data(self.graphData.nodes, function(node) {
          return node.name;
        })
        .enter()
        .append('g')
        .attr('id', function(node) {
          return 'label-' + node.index;
        });

      self.labels.append('text')
        .attr('x', '-2em')
        .attr('y', '-0.3em')
        .attr('pointer-events', 'none')
        .attr('id', function(node) {
          return 'label-shadow-' + node.index;
        })
        .attr('class', 'node-shadow')
        .text(function(node) {
          return node.name;
        });

      self.labels.append('text')
        .attr('x', '-2em')
        .attr('y', '-0.3em')
        .attr('pointer-events', 'none')
        .attr('id', function(node) {
          return 'label-f-' + node.index;
        })
        .attr('class', 'node-label')
        .text(function(node) {
          return node.name;
        });
      var safety = 0;
      while (fGraph.alpha() > 0.01) { // You'll want to try out different, "small" values for this
        fGraph.tick();
        if (safety++ > 500) {
          break; // Avoids infinite looping in case this solution was a bad idea
        }
      }
      fGraph.on('tick', function() {
        self.graphReposition(undefined, undefined, 'tick');
      });
    },

    /* semantic zoom, nodes don't change size, but spread out or strech */
    graphZoom: function(increment) {
      var self = this;
      var newZoom;
      if (increment === undefined) {
        newZoom = d3.event.scale;
      } else {
        self.zoomScale(self.currentZoom + increment);
      }

      if (self.currentZoom == newZoom) {
        // zoom is not chaged
        return;
      }

      // check if the 'show' threshold was crossed
      if (self.currentZoom < self.showThreshhold && newZoom >= self.showThreshhold) {
        self.svg.selectAll('g.label').classed('on', true);
      } else if (self.currentZoom >= self.showThreshhold && newZoom < self.showThreshhold) {
        self.svg.selectAll('g.label').classed('on', false);
      }

      // cumpute graph window size
      var vp = Yelp.Utils.ViewportSize();
      var width, height, newOffset;
      var zoomRatio = newZoom / self.currentZoom;

      if (vp.w < self.diameter) {
        width = vp.w;
      } else {
        width = self.diameter;
      }

      if (vp.h < self.diameter) {
        height = vp.h;
      } else {
        height = self.diameter;
      }

      newOffset = {
        x: self.currentOffset.x * zoomRatio + width / 2 * (1 - zoomRatio),
        y: self.currentOffset.y * zoomRatio + height / 2 * (1 - zoomRatio),
      };

      self.graphReposition(newOffset, newZoom, 'zoom');
    },

    graphDrag: function() {
      var offset = {
        x: this.currentOffset.x + d3.event.dx,
        y: this.currentOffset.y + d3.event.dy,
      };
      this.graphReposition(offset, undefined, 'drag');
    },

    graphReposition: function(offset, zoom, event) {
      // transition on move event
      var self = this;
      var transition = false;
      if (event == 'move') {
        transition = true;
      }

      if (offset !== undefined &&
        (offset.x != self.currentOffset.x || offset.y != self.currentOffset.y)) {
        var g = d3.select('g.graph-parent');
        if (transition) {
          g = g.transition.duration(500);
        }
        g.attr('transform', function() {
          return 'translate(' + offset.x + ',' + offset.y + ')';
        });
        self.currentOffset.x = offset.x;
        self.currentOffset.y = offset.y;
      }

      // get new zoom value
      if (zoom === undefined) {
        if (event != 'tick') {
          return;
        }
        zoom = self.currentZoom;
      } else {
        self.currentZoom = zoom;
      }

      var edge;
      if (transition) {
        edge = self.edges.transition().duration(500);
      } else {
        edge = self.edges;
      }

      edge
        .attr("x1", function(edg) {
          return zoom * edg.source.x;
        })
        .attr("y1", function(edg) {
          return zoom * edg.source.y;
        })
        .attr("x2", function(edg) {
          return zoom * edg.target.x;
        })
        .attr("y2", function(edg) {
          return zoom * edg.target.y;
        });

      var node;
      if (transition) {
        node = self.nodes.transition().duration(500);
      } else {
        node = self.nodes;
      }

      node.attr("transform", function(node) {
        return 'translate(' + zoom * node.x + ',' + zoom * node.y + ')';
      });

      var labels;
      if (transition) {
        labels = self.labels.transition().duration(500);
      } else {
        labels = self.labels;
      }

      labels.attr('transform', function(label) {
        return 'translate(' + zoom * label.x + ',' + zoom * label.y + ')';
      });
    },

    nodeHighlight: function(node, activate, set) {
      var self = this;

      if (activate && this.activeNode !== undefined) {
        this.nodeHighlight(self.graphData.nodes[self.activeNode], false, false);
      }

      var circle = d3.select('#node-' + node.index);
      var label = d3.select("#label-" + node.index);
      if (activate == false) {
      }
      label.classed('on', activate || this.currentZoom >= this.showThreshhold);
      label.selectAll('text').classed('main', activate);


      // activate all siblings
      Object(node.links).forEach(function(id) {

        d3.select("#node-" + id).classed('sibling', activate);
        var label = d3.select('#label-' + id);
        label.classed('on', activate || self.currentZoom >= self.showThreshhold);
        label.selectAll('text.node-label')
          .classed('sibling', activate);
      });
      self.activeNode = activate ? node.index : undefined;
      if (self.activeNode !== undefined) {
        self.graphData.nodes[self.activeNode].activate = set ? true : false;
      }
    },

    nodeModal: function(node) {
      var self = this;
      this.$content.prepend(this.modal({
        height: this.diameter,
        id: 'modalSelect'
      }));
      this.svg.attr('display', 'none');
      this.$content.find('.el').click(function(el) {

        var route = '';


        switch ($(this).attr('data-type')) {
          case "details":
            route = 'yusers/' + node.id;
            break;
          case "friends-graph":
            route = '/yusers/friends-graph/' + node.id;
            break;
          default:
            self.$content.find('#modalSelect').remove();
            self.svg.attr('display', null);
            return;
            break;
        }
        self.setNewRoute(route);
      });
    },

    setNewRoute: function(route) {
      if (this.routing === false) {
        this.routing = true;
        Yelp.Router.setRoute(route);
        this.routing = false;
      }
    },
  });
}();