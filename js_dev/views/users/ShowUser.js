var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.ShowUser = Yelp.Views.BaseView.extend({
    model: Yelp.Models.YUser,
    tagName: 'div',
    // 
    template: Yelp.template('userTemplate'),
    friendsTemplate: Yelp.template('userFriendsTemplate'),

    initialize: function() {
      Yelp.Views.ShowUser.__super__.initialize.call(this);
      Yelp.Events.on('yusers:show', this.show, this);
      _.bindAll(this, 'show', 'render', 'getTemplate', 'bindEvents', 'userComplimentsEvent', 'userFriendsEvent',
        'reviewTotalNumberChart', 'userStatisticsEvent', 'userReviewsEvent', 'userStatisticsButton', 'closeModal',
        'createReviewNumberChart', 'userMapEvent'
      );
    },
    routing: false,

    show: function(id) {
      this.changed = false;
      var self = this;
      this.$content.empty();
      this.model.set('user_id', id);
      this.model.fetch({
        success: function() {
          self.$content.append(self.$el.hide());
          self.render();
          if (!self.changed) {
            self.$content.append(self.$el);
            self.$el.show();
            self.showUserFriends();
            self.userComplimentsEvent();
            self.userStatisticsEvent();
            self.mapIsRendered = false;
            Yelp.Loading.close();
          } else
            self.$el.empty();
        },
      });
    },

    render: function() {
      var self = this;
      self.$el.html(self.getTemplate());
      self.bindEvents();
      return self;
    },

    bindEvents: function() {
      var self = this;
      self.$el.find('#userFriendsButton').click(self.userFriendsEvent);
      self.$el.find('#userComplimentsButton').click(function() {
        self.$('#userComplimentsModal').parent().toggleClass('user-margin-down');
        self.$('#userComplimentsModal').toggle();
        if (self.$('#userComplimentsModal #modalContent').html() === '') {
          Yelp.Loading.open();
        }
      });
      self.$el.find('#userReviewsButton').click(self.userReviewsEvent);
      self.$el.find('#userStatisticsButton').click(self.userStatisticsButton);
		self.$el.find('#userMapButton').click(self.userMapEvent);
    },

    userStatisticsButton: function() {
      var self = this;
      var rendered = true;
      var $children = self.$('#userStatisticsModal').children('#modalContent').children();
      $children.each(function() {
        if (this.innerHTML.trim() === '') {
          rendered = false;
        }
      });
      if (rendered === true) {
        Yelp.Loading.close();
        self.$('#userStatisticsModal').children('#modalContent').children().toggle();
      } else {
        _.delay(self.closeModal, 200);
        self.$('#userStatisticsModal').children('#modalContent').children().show().addClass('keep_opened');
        Yelp.Loading.open();
      }
    },

    closeModal: function() {
      var self = this;
      var rendered = true;
      var $children = self.$('#userStatisticsModal').children('#modalContent').children();
      $children.each(function() {
        if (this.innerHTML.trim() === '') {
          rendered = false;
        }
      });
      if (rendered === true) {
        Yelp.Loading.close();
      } else {
        _.delay(self.closeModal, 200);
      }
    },

    userFriendsEvent: function(event) {
      this.setNewRoute('/yusers/friends-graph/' + this.model.get('user_id'));
    },

    setNewRoute: function(route) {
      if (this.routing === false) {
        this.routing = true;
        Yelp.Router.setRoute(route);
        this.routing = false;
      }
    },

    showUserFriends: function() {
      var self = this;
      var $friendsModal = self.$("#userFriendsModal");
      $.ajax({
        url: '/api/yusers?withFriends=1&columns[]=friends&friendsColumns[]=name&friendsColumns[]=user_id',
        type: 'GET',
        dataType: 'json',
        data: {
          'filter[user_id]': this.model.get('user_id'),
        },
      })
        .done(function(data) {
          // sort user friends
          if (data.hasOwnProperty(0) && data[0].friends.length !== 0) {
            var names = [];
            var map = {};
            for (var key in data[0].friends) {
              names.push(data[0].friends[key].name);
              map[data[0].friends[key].name] = key;
            }
            var sortedFriends = [];
            names.sort();
            for (var i = 0, length = names.length; i < length; i++) {
              sortedFriends.push(data[0].friends[map[names[i]]]);
            }
            data[0].friends = sortedFriends;
          }
          $friendsModal.find('#modalContent').html(self.friendsTemplate({
            user: data
          }));
          $friendsModal.find('#modalContent').find('a').click(function(event) {
            Yelp.Router.setRoute('yusers/' + $(this).attr('data-id'));
          });
          Yelp.Loading.close();
        })
        .fail(function() {
          console.log("error");
          Yelp.Loading.close();
        });
    },

    userComplimentsEvent: function() {
      var self = this;
      var $parent = $('#userComplimentsModal');
      var $content = $parent.find('#modalContent');
      var data = self.model.getChartCompliments();
      if ($content.html().trim() !== '')
        return;

      if (data.length === 0) {
        $content.html('<h3 class="text-center">This user has no compliments</h3>');
        $parent.hide();
        return;
      }
      var newData = this.hideInsignificantData(data);
      data = newData;

      var diameter = Math.min($parent.width(), 400);
      $parent.hide();
      $parent.height(diameter);

      var svg = d3.select($content.get(0))
        .append('svg')
        .attr('class', 'centered')
        .attr('width', diameter)
        .attr('height', diameter)
        .append('g');
      svg.append('g').attr('class', 'compliments-slices');
      svg.append('g').attr('class', 'compliments-labels');
      svg.append('g').attr('class', 'compliments-lines');

      var midAngle = function(d) {
        return d.startAngle + (d.endAngle - d.startAngle) / 2;
      };

      var radius = diameter / 3;
      var pie = d3.layout.pie().sort(null).value(function(d) {
        return d.value;
      });

      var arc = d3.svg.arc()
        .outerRadius(radius * 0.6)
        .innerRadius(radius * 0.2);

      var color = d3.scale.category20();

      var outerArc = d3.svg.arc()
        .innerRadius(radius * 0.7)
        .outerRadius(radius * 0.7);

      svg.attr('transform', 'translate(' + diameter / 2 + ',' + diameter / 2 + ')');

      // slices 
      var slice = svg.select('.compliments-slices')
        .selectAll('path.compliments-slice')
        .data(pie(data), function(d) {
          return d.data.label;
        })
        .enter()
        .insert('path')
        .attr('class', 'compliments-slice')
        .style('fill', function(d, i) {
          return color(i);
        })
        .each(function(d) {
          this._current = d;
        });

      slice
        .transition()
        .attrTween("d", function(d) {
          var interpolate = d3.interpolate(this._current, d);
          var self = this;
          return function(t) {
            self._current = interpolate(t);
            return arc(self._current);
          };
        });

      // text
      var x = true;
      var text = svg.select('.compliments-labels')
        .selectAll('text')
        .data(pie(data), function(d) {
          return d.data.label;
        })
        .enter()
        .append('text')
        .attr('dy', '0.35em')
        .text(function(d) {
          return d.data.label + '(' + d.data.value + ')';
        })
        .each(function(d) {
          this._current = d;
        })
        .transition()
        .attrTween("transform", function(d) {
          var interpolate = d3.interpolate(this._current, d);
          var self = this;
          return function(t) {
            var d2 = interpolate(t);
            self._current = d2;
            var pos = outerArc.centroid(d2);
            pos[0] = radius * 0.70 * (midAngle(d2) < Math.PI ? 1 : -1);
            return "translate(" + pos + ")";
          };
        })
        .styleTween("text-anchor", function(d) {
          this._current = this._current || d;
          var interpolate = d3.interpolate(this._current, d);
          this._current = interpolate(0);
          return function(t) {
            var d2 = interpolate(t);
            return midAngle(d2) < Math.PI ? "start" : "end";
          };
        });

      var polyline = svg.select(".compliments-lines").selectAll("polyline")
        .data(pie(data), function(d) {
          return d.data.label;
        })
        .enter()
        .append("polyline")
        .each(function(d) {
          this._current = d;
        })
        .transition()
        .style("opacity", function(d) {
          return d.data.value === 0 ? 0 : 0.5;
        })
        .attrTween("points", function(d) {
          this._current = this._current;
          var interpolate = d3.interpolate(this._current, d);
          var self = this;
          return function(t) {
            var d2 = interpolate(t);
            self._current = d2;
            var pos = outerArc.centroid(d2);
            pos[0] = radius * 0.65 * (midAngle(d2) < Math.PI ? 1 : -1);
            return [arc.centroid(d2), outerArc.centroid(d2), pos];
          };
        });
      Yelp.Loading.close();
    },

    userReviewsEvent: function(event) {
      // Yelp.Router.setRoute('yreviews', {
      //   filter: {
      //     user_id: this.model.get('user_id'),
      //   }
      // });
    },

    userStatisticsEvent: function(event) {
      var self = this;
      var $modal = self.$('#userStatisticsModal');
      self.reviewTotalNumberChart($modal.find('#reviewTotalNumberChart'));
      self.reviewStats($modal.find('#reviewCityChart'));


    },

    reviewTotalNumberChart: function($el) {
      var self = this;
      $.ajax({
        url: 'api/yusers/reviewstars',
        type: 'GET',
        dataType: 'json',
        data: {
          'user_id': this.model.get('user_id')
        },
      })
        .done(function(data) {
          data.sort(function(a, b) {
            return parseInt(a.stars, 10) - parseInt(b.stars, 10);
          });
          self.createReviewNumberChart($el, data);
        })
        .fail(function() {
          $el.html('<h3 class="text-center">This user was not found</h3>');
        });
    },

    createReviewNumberChart: function($el, data) {

      var self = this;
      var margin = {
        top: 40,
        right: 20,
        bottom: 30,
        left: 40
      };
      var width = $el.get(0).offsetWidth - margin.left - margin.right;
      var height = 220 - margin.top - margin.bottom;
      var x = d3.scale.ordinal().rangeRoundBands([0, width], 0.1);
      var y = d3.scale.linear().range([height, 0]);
      var xAxis = d3.svg.axis().scale(x).orient('bottom');
      var yAxis = d3.svg.axis().scale(y).orient('left').ticks(10, '');
      var tooltip = d3.tip()
        .attr('class', 'review-number-tooltip')
        .offset([-10, 0])
        .html(function(d) {
          return '<strong>Reviews:</strong><span style="color:red">' + d.reviews + '</span>';
        });

      var svg = d3.select($el.get(0)).append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
      if (!$el.hasClass('keep_opened')) {
        $el.hide();
      }

      svg.call(tooltip);

      x.domain(data.map(function(d) {
        return d.stars + '';
      }));
      y.domain([0, d3.max(data, function(d) {
        return parseInt(d.reviews, 10);
      })]);

      svg.append('g')
        .attr('class', 'review-number-x review-number-axis')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis);

      svg.append('g')
        .attr('class', 'review-number-y review-number-axis')
        .call(yAxis)
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.2em')
        .style('text-anchor', 'end')
        .text('Number of reviews');

      svg.selectAll('.review-number-bar')
        .data(data)
        .enter()
        .append('rect')
        .attr('class', 'review-number-bar')
        .attr('x', function(d) {
          return x(d.stars);
        })
        .attr('width', x.rangeBand())
        .attr('y', function(d) {
          return y(parseInt(d.reviews));
        })
        .attr('height', function(d) {
          return height - y(parseInt(d.reviews, 10));
        })
        .on('mouseover', tooltip.show)
        .on('mouseout', tooltip.hide);

    },

    reviewStats: function($el) {
      var self = this;
      $.ajax({
        url: '/api/yusers/reviewstats',
        type: 'GET',
        dataType: 'json',
        data: {
          'user_id': this.model.get('user_id')
        },
      })
        .done(function(data) {
          var clonedData = $.extend(true, {}, data, 'nr_reviews', 'Reviews');
          self.reviewDataChart($el, data);
        });
    },

    reviewDataChart: function($el, data) {
      var self = this;
      var margin = {
        top: 40,
        right: 30,
        bottom: 100,
        left: 40
      };

      var width = $el.get(0).offsetWidth - margin.left - margin.right;
      var height = 400 - margin.top - margin.bottom;

      var x = d3.scale.ordinal().rangeRoundBands([0, width], 0.1);

      var y0 = d3.scale.linear().range([height, 0]);
      var y1 = d3.scale.linear().range([height, height / 2]);

      var xAxis = d3.svg.axis().scale(x).orient('bottom');
      var yAxisLeft = d3.svg.axis().scale(y0).orient('left').ticks(10);
      var yAxisRight = d3.svg.axis().scale(y1).orient('right').ticks(10);

      var tooltipLeft = d3.tip()
        .attr('class', 'review-number-tooltip')
        .offset([-10, 0])
        .html(function(d) {
          return '<strong>Number of Reviews: </strong><span style="color:red">' + d.nr_reviews + '</span>';
        });

      var tooltipRight = d3.tip()
        .attr('class', 'review-number-tooltip')
        .offset([-10, 0])
        .html(function(d) {
          return '<strong>Average Stars: </strong><span style="color:red">' + d.avg_stars + '</span>';
        });

      var svg = d3.select($el.get(0)).append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
      if (!$el.hasClass('keep_opened')) {
        $el.hide();
      }
      svg.call(tooltipLeft);
      svg.call(tooltipRight);

      x.domain(data.map(function(d) {
        return d.city + '';
      }));

      y0.domain([0, d3.max(data, function(d) {
        return parseInt(d.nr_reviews, 10);
      })]);

      y1.domain([0, 5]);

      svg.append('g')
        .attr('class', 'review-number-x review-number-axis')
        .attr('transform', 'translate(0, ' + height + ')')
        .call(xAxis)
        .selectAll("text")
        .style("text-anchor", "end")
        .attr("dx", "-.8em")
        .attr("dy", ".15em")
        .attr("transform", 'rotate(-65)');

      svg.append('g')
        .attr('class', 'review-number-y review-number-axis')
        .call(yAxisLeft)
        .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.2em')
        .style('text-anchor', 'end')
        .text('Number of reviews');

      svg.append("g")
        .attr("class", "review-number-y review-number-axis review-number-axis-right")
        .attr("transform", "translate(" + (width) + ",0)")
        .call(yAxisRight)
        .append("text")
        .attr('transform', 'rotate(-90)')
        .attr('y', -10)
        .attr('dy', '0.71em')
        .style('text-anchor', 'end')
        .text('Average Review Stars');

      var bars = svg.selectAll('.review-number-bar')
        .data(data)
        .enter();

      bars.append('rect')
        .attr('class', 'review-number-bar')
        .attr('x', function(d) {
          return x(d.city);
        })
        .attr('width', x.rangeBand() / 2)
        .attr('y', function(d) {
          return y0(parseInt(d.nr_reviews));
        })
        .attr('height', function(d) {
          return height - y0(parseInt(d.nr_reviews, 10));
        })
        .on('mouseover', tooltipLeft.show)
        .on('mouseout', tooltipLeft.hide);


      bars.append('rect')
        .attr('class', 'review-number-bar-second')
        .attr('x', function(d) {
          return x(d.city) + x.rangeBand() / 2;
        })
        .attr('width', x.rangeBand() / 2)
        .attr('y', function(d) {
          return y1(parseInt(d.avg_stars, 10));
        })
        .attr('height', function(d) {
          return height - y1(parseInt(d.avg_stars, 10));
        })
        .on('mouseover', tooltipRight.show)
        .on('mouseout', tooltipRight.hide);
    },

    userMapEvent: function(event) {
      var target = $(event.target);

      this.mapIsRendered = this.mapIsRendered || false;

      if(this.mapIsRendered === false)
      {
      	Yelp.Loading.open();
        var mapUserView = new Yelp.Views.UserMaps({
          collection: new Yelp.Collections.YUserBusiness(),
          user_id : this.model.get('user_id')
        });

        mapUserView.show();
        //console.log("hit");
        this.mapIsRendered = true;
        Yelp.Loading.close();
      }

      $('#userMap').toggle();

    },

    getTemplate: function() {
      var fans = this.model.get('fans') + ' fan';
      var friends = this.model.get('friends').length + ' friend';
      var compliments = this.model.getTotalCompliments() + ' compliment';
      var reviews = this.model.get('review_count') + ' review';
      fans += this.model.get('fans') != 1 ? 's' : '';
      friends += this.model.get('friends') != 1 ? 's' : '';
      compliments += this.model.getTotalCompliments() != 1 ? 's' : '';
      reviews += this.model.get('review_count') != 1 ? 's' : '';

      return this.template({
        name: this.model.get('name'),
        fans: fans,
        friends: friends,
        compliments: compliments,
        reviews: reviews,
        avgStars: this.model.get('average_stars'),
        eliteYears: this.model.get('elite').join(', '),
        yelpSince: this.model.get('yelp_since'),
      });
    },

    hideInsignificantData: function(data) {
      var med = 1;
      var changed = false;
      for (var label in data) {
        med += data[label].value;
        med /= 2;
      }

      var newData = [];
      for (label in data) {
        if (data[label].value / med * 100 < 12) {
          delete(data[label]);
        } else {
          newData.push(data[label]);
        }
      }
      if (changed) {
        return this.hideInsignificantData(newData);
      }
      return newData;
    }
  });
}();