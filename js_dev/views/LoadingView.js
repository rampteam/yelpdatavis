var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  /**
   * Base class for all item views
   */
  Yelp.Views.LoadingView = Backbone.View.extend({
    diameter: 640,
    $content: null,
    contentName: null,
    changed: false,
    initialize: function(options) {
      this.$content = $('#loadingModal').foundation('reveal');
    },

    open: function () {
      this.$content.foundation('reveal', 'open');
    },

    close: function() {
      this.$content.foundation('reveal', 'close');
    }

  });
}();