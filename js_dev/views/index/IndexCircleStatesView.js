var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.IndexCircleStatesView = Yelp.Views.BaseView.extend({
    template: Yelp.template('indexModal'),

    tagName: 'div',
    // svg 
    color: null,

    graphData: undefined,
    nodes: undefined,
    activeNode: undefined,
    fGraph: null,
    svg: undefined,

    currentOffset: {
      x: 0,
      y: 0,
    },
    currentZoom: 1.0,
    margin: 10,

    // 
    routing: false,
    country: undefined,
    state: undefined,

    initialize: function(models, options) {
      Yelp.Events.on('index:circle', this.show, this);
      Yelp.Views.IndexCircleStatesView.__super__.initialize.call(this);

      _.bindAll(this, 'render', 'buildSvg', 'setNewRoute', 'graphDrag', 'countryClickEvent', 'stateClickEvent', 'cityClickEvent');
    },

    setSvgInfo: function() {
      this.svg = undefined;
      this.currentOffset = {
        x: 0,
        y: 0,
      };
      this.currentZoom = 1.0;
      this.activeNode = undefined;

      this.xyScale = d3.scale.linear().domain([0, this.diameter]).range([0, this.diameter]);
      this.zoomScale = d3.scale.linear().domain([1, 6]).range([1, 6]).clamp(true);
      this.circleSize = d3.scale.linear().domain([50, 100]).range([this.diameter / 8, this.diameter / 4]).clamp(true);

      this.color = d3.scale.linear()
        .domain([-1, 5])
        .range(['hsl(152,80%,80%)', 'hsl(228,30%,40%)'])
        .interpolate(d3.interpolateHcl);
      // containement to represent the hierarchy, each element size will be determined by children size (leaf size eventualy)
      this.fGraph = d3.layout.force()
        .charge(-(5 * this.diameter))
        .size([this.diameter - this.margin, this.diameter - this.margin])
        .linkStrength(0.1)
        .gravity(0.2)
        .nodes(this.graphData)
        .start();
    },

    show: function(country, state) {
      var self = this;
      this.changed = false;
      this.collection.fetch({
        success: function() {
          self.render(country, state)
        }
      });
    },

    render: function(country, state) {
      this.country = country;
      this.state = state;

      this.$content.empty();
      this.$content.append(this.$el.empty());

      if (state) {
        this.buildSvg(this.getStateJson(country, state), this.cityClickEvent);
      } else if (country) {
        this.buildSvg(this.getCountryJson(country), this.stateClickEvent);
      } else {
        this.buildSvg(this.getStatesJson(), this.countryClickEvent);
      }

      if (this.changed) {
        this.$el.empty();
        Yelp.Loading.close();
      } else {
        this.$el.show();
        Yelp.Loading.close();
      }
      return this;
    },

    buildSvg: function(svgJson, clickEventFn) {
      // build svg json
      var self = this;
      var view;
      self.graphData = svgJson;
      this.setSvgInfo();


      // create svg and append a container
      this.svg = d3.select(self.el).append('svg')
        .attr('width', self.diameter)
        .attr('height', self.diameter)
        .attr("viewBox", "0 0 " + self.diameter + " " + self.diameter)
        .attr("preserveAspectRatio", "xMidYMid meet")
        .attr('class', 'users-graph');

      self.svg.call(
        d3.behavior.drag().on('drag', self.graphDrag)
      );

      var graph = self.svg.append('g').attr('class', 'graph-parent');

      this.nodes = graph.append('g')
        .attr('class', 'graph graph-nodes')
        .selectAll('circle')
        .data(svgJson, function(node) {
          return node.name;
        })
        .enter()
        .append('g');
      this.nodes.append('circle')
        .attr('class', function(node) {
          return 'level-' + Math.floor((Math.random() * 3) + 1);
        })
        .attr('id', function(node) {
          return 'node-' + node.index;
        })
        .attr('r', function(node) {
          return self.circleSize(50);
        })
        .attr('pointer-events', 'all')
        .on('click', function(node) {
          if (node.activate !== true) {
            self.nodeHighlight(node, true, true);
          } else {
            clickEventFn(node);
          }
        })
        .on('mouseover', function(node) {
          self.nodeHighlight(node, true);
        })
        .on('mouseleave', function(node) {
          self.nodeHighlight(node, false);
        });

      this.nodes
        .append('text')
        .attr('dy', '0.35em')
        .attr('pointer-events', 'none')
        .attr('text-anchor', "middle")
        .attr('id', function(node) {
          return 'label-' + node.index;
        })
        .attr('class', 'graph graph-label')
        .text(function(node) {
          return node.name;
        });
      var safety = 0;
      while (this.fGraph.alpha() > 0.01) { // You'll want to try out different, "small" values for this
        this.fGraph.tick();
        if (safety++ > 500) {
          break; // Avoids infinite looping in case this solution was a bad idea
        }
      }

      this.fGraph.on('tick', function() {
        self.graphReposition(undefined, undefined, 'tick');
      });

      return this;
    },

    graphDrag: function() {
      var offset = {
        x: this.currentOffset.x + d3.event.dx,
        y: this.currentOffset.y + d3.event.dy,
      };
      this.graphReposition(offset, undefined, 'drag');
    },

    graphReposition: function(offset, zoom, event) {

      // transition on move event
      var self = this;
      var transition = false;
      if (event == 'move') {
        transition = true;
      }

      if (offset !== undefined && (offset.x != self.currentOffset.x || offset.y != self.currentOffset.y)) {
        var g = d3.select('g.graph-parent');
        if (transition) {
          g = g.transition.duration(500);
        }
        g.attr('transform', function() {
          return 'translate(' + offset.x + ',' + offset.y + ')';
        });
        self.currentOffset.x = offset.x;
        self.currentOffset.y = offset.y;
      }

      // get new zoom value
      if (zoom === undefined) {
        if (event != 'tick') {
          return;
        }
        zoom = self.currentZoom;
      } else {
        self.currentZoom = zoom;
      }


      var node;
      if (transition) {
        node = this.nodes.transition();
      } else {
        node = this.nodes;
      }

      node.attr("transform", function(node) {
        return 'translate(' + zoom * node.x + ',' + zoom * node.y + ')';
      });
    },

    nodeHighlight: function(node, activate, set) {
      var self = this;

      if (activate && this.activeNode !== undefined) {
        this.nodeHighlight(self.graphData[self.activeNode], false);
      }
      var circle = d3.select('#node-' + node.index);
      var label = d3.select("#label-" + node.index);
      circle.classed('main', activate);
      label.classed('on', activate);
      label.selectAll('text').classed('main', activate);
      node.activate = set ? true : false;
      self.activeNode = activate ? node.index : undefined;
    },

    getStateJson: function(country, state) {
      var data = this.getSvgJson().filter(function(obj) {
        return obj.state == country;
      });
      data = data[0].children.filter(function(obj) {
        return obj.name == state;
      });
      if (data.length === 0) {
        Yelp.Router.setRoute('NotFound');
        return
      }

      var svgJson = [];
      var i = 0;
      _.each(data[0].cities, function(value, key, list) {
        svgJson.push({
          'name': value,
          'index': i++
        });
      });

      return svgJson;
    },

    getCountryJson: function(country) {
      var data = this.getSvgJson().filter(function(obj) {
        return obj.state == country;
      });
      if (data.length === 0) {
        Yelp.Router.setRoute('NotFound');
        return
      }
      var svgJson = [];
      var i = 0;
      _.each(data[0].children, function(value, key, list) {
        svgJson.push({
          'name': value.name,
          'index': i++
        });
      });
      return svgJson;
    },

    getStatesJson: function() {
      var svgJson = [];
      var i = 0;
      _.each(this.getSvgJson(), function(value, key, list) {
        svgJson.push({
          'name': value.state,
          'index': i++
        });
      });
      return svgJson;
    },

    getSvgJson: function() {
      var svgJson = [];
      this.collection.each(function(el, index) {
        svgJson.push(el.getSvgInfo());
      });
      return svgJson;
    },

    cityClickEvent: function(node) {
      var self = this;
      this.$content.prepend(this.template({
        height: this.diameter,
        id: 'modalSelect'
      }));
      this.svg.attr('display', 'none');
      this.$content.find('.el').click(function(el) {

        var route = '';
        var state = encodeURIComponent(self.state);
        var city = encodeURIComponent(node.name);
        var filters = {
          filter: {
            city: city,
            state: state,
          }
        };

        switch ($(this).attr('data-type')) {
          case "businesses":
            route = 'ybusinesses/maps';
            break;
          case "user":
            route = 'yusers';
            filters.limit = 250;
            filters.orderBy = {
              'review_count': 'DESC',
            };
            break;
          default:
            self.$content.find('#modalSelect').remove();
            self.svg.attr('display', null);
            return;
            break;
        }
        self.setNewRoute(route, filters);
      });
    },

    stateClickEvent: function(node) {
      var route = 'index/country/' + this.country + '/state/' + node.name;
      this.setNewRoute(route);
    },

    countryClickEvent: function(node) {
      var route = 'index/country/' + node.name;
      this.setNewRoute(route);
    },

    setNewRoute: function(route, filters) {
      if (this.routing === false) {
        this.routing = true;
        Yelp.Router.setRoute(route, filters);
        this.routing = false;
      }
    },



  });
}();