var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.OrderByFilter = Yelp.Views.BaseView.extend({
    model: Yelp.Models.Filter,
    tagName: 'div',
    // 
    template: Yelp.template('optionTemplate'),

    initialize: function() {
      Yelp.Views.OrderByFilter.__super__.initialize.call(this);
      this.model.set('type', 'orderBy');
      this.model.set('active', false);
    },

    render: function() {
      var self = this;
      var options = '' + this.getOption('ASC') + this.getOption('DESC') +
        '<div class="option reset-filter">Remove</div>';

      self.$el.html(self.template({
        'name': self.model.get('label'),
        'options': options,
      }));
      return self.bindEvents();
    },

    getOption: function(type) {
      var el = document.createElement('div');
      el.className = 'option';
      el.innerHTML = type;
      el.setAttribute('value', type);
      if (type == this.model.get('value')) {
        el.className += ' active';
      }
      var tmp = document.createElement('div');
      tmp.appendChild(el);
      return tmp.innerHTML;
    },

    bindEvents: function() {
      var self = this;
      var $label = self.$('.search-filter-label');
      var $content = self.$('.search-filter-content');
      var $back = self.$('.search-filter-content .back');
      var $options = self.$('.search-filter-content .option');

      $label.on('click', function(event) {
        event.preventDefault();
        $label.closest('.search-filter').siblings().hide();
        $label.hide();
        $content.addClass('visible');
      });

      $back.on('click', function(event) {
        $label.closest('.search-filter').siblings().show();
        event.preventDefault();
        $label.show();
        $content.removeClass('visible');
      });

      $options.on('click', function(event) {
        event.preventDefault();
        var $el = $(this);

        if ($el.hasClass('reset-filter')) {
          self.model.set('value', '');
        } else {
          self.model.set('value', $el.attr('value'));
          $options.removeClass('active');
          $el.addClass('active');
        }
        $back.trigger('click');
      });
      return self;
    },
  });
}();