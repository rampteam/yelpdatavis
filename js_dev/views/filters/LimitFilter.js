var Yelp = Yelp || {};
Yelp.Views = Yelp.Views || {};

! function() {

  "use strict";

  Yelp.Views.LimitFilter = Yelp.Views.BaseView.extend({
    model: Yelp.Models.Filter,
    tagName: 'div',
    // 
    template: Yelp.template('optionTemplate'),

    initialize: function() {
      Yelp.Views.LimitFilter.__super__.initialize.call(this);
      this.model.set('type', 'limit');
    },

    render: function() {
      var self = this;
      var options = '';
      var limit = this.model.get('limit');
      var currentLimit = this.model.get('value');
      for (var i = 0, length = limit.length; i < length; i++) {
        var el = document.createElement('div');
        el.className = 'option';
        el.innerHTML = limit[i];
        el.setAttribute('value', limit[i]);
        if (limit[i] == currentLimit) {
          el.className += ' active';
        }
        var tmp = document.createElement('div');
        tmp.appendChild(el);
        options += tmp.innerHTML;
      }
      self.$el.html(self.template({
        'name': self.model.get('label'),
        'options': options,
      }));
      return self.bindEvents();
    },

    bindEvents: function() {
      var self = this;
      var $label = self.$('.search-filter-label');
      var $content = self.$('.search-filter-content');
      var $back = self.$('.search-filter-content .back');
      var $options = self.$('.search-filter-content .option');

      $label.on('click', function(event) {
        $label.closest('.search-filter').siblings().hide();
        $label.hide();
        $content.addClass('visible');
      });

      $back.on('click', function(event) {
        $label.closest('.search-filter').siblings().show();
        Yelp.Events.trigger('showSearch');
        $label.show();
        $content.removeClass('visible');
      });

      $options.on('click', function(event) {
        var $el = $(this);
        self.model.set('value', parseInt($el.attr('value'), 10));
        $el.closest('.search-filter').siblings().hide();
        $options.removeClass('active');
        $el.addClass('active');
        $back.trigger('click');
      });
      return self;
    },
  });
}();